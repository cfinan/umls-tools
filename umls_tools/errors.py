"""
Error classes for the UMLS package
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GeniaParserError(Exception):
    """
    Called when a there is a problem with a row from one of the GWAS
    repositories
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, *args):
        """
        Initialise

        Paramerters
        -----------

        message : :obj:`str`
            The error message to output
        """

        # Store attributes
        self.message = message

        # Call the Exception superclass
        super(GeniaParserError, self).__init__(message, *args)
