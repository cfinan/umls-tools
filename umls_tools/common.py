"""Common variants for use with diffent scrips and APIs in the main and
sub-packages.
"""
import os
import sys


try:
    DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db.cnf')
    """The default fallback path for the config file, root of the home
    directory (`str`)
    """
except KeyError:
    # HOME environment variable does not exist for some reason
    _DEFAULT_CONFIG = None


DEFAULT_SECTION = 'umls'
"""The default section name in an uni config file that contains SQLAlchemy
connection parameters (`str`)
"""

DEFAULT_PREFIX = 'db.'
"""The default prefix name in an ini config section each SQLAlchemy connection
parameter should be prefixed with this (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def overlap_1d(line1, line2, touch_is_overlap=False):
    """determine if line1 or line2 overlap in a 1d plane. Note that: if max
    line1 == min line2 (or visa versa) then this is not considered an overlap.

    Parameters
    ----------
    line1 : `tuple` of `int` or `float`
        The min [0] and max [1] boundaries of the first line.
    line2 : `tuple` of `int` or `float`
        The min [0] and max [1] boundaries of the second line.
    touch_is_overlap : `bool`
        If the lines are touching i.e. the end of one == the start of another
        then this is reported as an overlap wit the smallest unit available. so
        for ints this would be 1 and floats it is the smallest float available.

    Returns
    -------
    overlap : `int` or `float`
        The amount of overlap or 0 if there is no overlap.
    """
    is_int = True
    zero = 0

    # Are we dealing with all ints, if so we are ok, otherwise we have to make
    # everything floats
    if not all([isinstance(i, int) for i in line1]) or \
       not all([isinstance(i, int) for i in line2]):
        line1 = tuple([float(i) for i in line1])
        line2 = tuple([float(i) for i in line2])
        zero = 0.0
        is_int = False

    min1, max1 = line1
    min2, max2 = line2

    if touch_is_overlap is True:
        if is_int is True:
            min1 -= 1
            min2 -= 1
        else:
            min1 -= min1 * sys.float_info.epsilon
            min2 -= min2 * sys.float_info.epsilon

    return max(zero, min(max1, max2) - max(min1, min2))
