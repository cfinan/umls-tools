"""Here are the actual test for the module: run with ``pytest test_module.py``.
There is no need to import conftest it happens automatically.
"""
from collections import namedtuple
from umls_tools.admin import index
import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_str,exp_res",
    # Each element in this list represents a single parameter
    [
        (
            "A",
            [
            ],
        ),
        (
            "AA",
            [
                ((0, 2), 0, 'aa'),
            ],
        ),
        (
            "AA BB CC DD EE FF GG",
            [
                ((0, 2), 0, 'aa'),
                ((3, 5), 1, 'bb'),
                ((6, 8), 2, 'cc'),
                ((9, 11), 3, 'dd'),
                ((12, 14), 4, 'ee'),
                ((15, 17), 5, 'ff'),
                ((18, 20), 6, 'gg')
            ]
        ),
        (
            "Acute or sub-acute loss of plasma volume e.g. in burns,"
            " pancreatitis, trauma, and complications of surgery (with"
            " isotonic solutions),",
            [((0, 5), 0, 'acut'),
             ((9, 12), 2, 'sub'),
             ((13, 18), 3, 'acut'),
             ((19, 23), 4, 'loss'),
             ((27, 33), 6, 'plasma'),
             ((34, 40), 7, 'volum'),
             ((41, 42), 8, 'e'),
             ((43, 44), 9, 'g'),
             ((49, 54), 11, 'burn'),
             ((56, 68), 12, 'pancreat'),
             ((70, 76), 13, 'trauma'),
             ((82, 95), 15, 'complic'),
             ((99, 106), 17, 'surgeri'),
             ((113, 121), 19, 'isoton'),
             ((122, 131), 20, 'solut')],
        )
    ]
)
def test_process_str(test_str, exp_res):
    """Here we are using a fixture from our conftest.py (return_fixture) and the
    test parameters. Run pytest with the -s flag to see print statements.
    """
    # for i in range(len(test_str)):
    #     print(i, test_str[i])
    res_str = index.IndexTerms.process_str(test_str)
    assert res_str == exp_res, "wrong result"


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RunTestSearchIndex(index.BaseSearchIndex):
    """Mock class to test the index searching.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, index_str):
        pk = 1
        TermIndexLookup = namedtuple(
            "TermIndexLookup",
            ['token_id', 'token_str', 'token_count', 'token_prob']
        )
        TermIndexMap = namedtuple(
            "TermIndexMap",
            ['term_id', 'term_len']
        )
        self.lookup = {}
        for idx, i in enumerate(index_str, 1):
            for pos, rank, text in index.IndexTerms.process_str(i):
                start, end = pos
                lkp = TermIndexLookup(pk, text, 10, 0.1)
                tmap = TermIndexMap(idx, len(i))

                try:
                    self.lookup[text].append((lkp, tmap))
                except KeyError:
                    self.lookup[text] = [(lkp, tmap)]
                pk += 1
        self.nterms = 100

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def query_index(self, token):
        """Query the index for the token.
        """
        try:
            for row in self.lookup[token]:
                yield row
        except KeyError:
            pass

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_db, test_str",
    # Each element in this list represents a single parameter
    [
        # (
        #     ("AA", "AA BB CC DD EE FF GG"), "XX CC EE"
        # ),
        (
                (
                    "Acute or sub-acute loss of plasma volume e.g. in burns,"
                    " pancreatitis, trauma, and complications of surgery (with"
                    " isotonic solutions),",
                ), "acute plasma volume, surgery"
        )
    ]
)
def test_search_term(test_db, test_str):
    """Here we are using a fixture from our conftest.py (return_fixture) and the
    test parameters. Run pytest with the -s flag to see print statements.
    """
    search = RunTestSearchIndex(test_db)
    result = search.search_term(test_str)
    print(result)
