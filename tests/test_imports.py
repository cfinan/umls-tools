"""Tests that all the modules in the package can be imported without error.
"""
import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'umls_tools.codes',
        'umls_tools.common',
        'umls_tools.errors',
        'umls_tools.log',
        'umls_tools.mapping',
        'umls_tools.objects',
        'umls_tools.orm_mixin',
        'umls_tools.orm',
        'umls_tools.parse',
        'umls_tools.query',
        'umls_tools.admin.build',
        'umls_tools.admin.index',
        'umls_tools.admin.output_sab',
        'umls_tools.admin.unique_cui',
        'umls_tools.metamap.metamap',
        'umls_tools.metamap.objects',
        'umls_tools.metamap.processes',
        'umls_tools.neo4j.extract',
        'umls_tools.neo4j.mapping',
        'umls_tools.rest.client',
        'umls_tools.similarity.neo4j',
        'umls_tools.similarity.similarityweb'
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)
