# `misc` directory
This contains assorted scripts that I have put together that are useful for reference but are not absolutely required for the package. They may get removed at anytime. If anything really useful gets developed then it will be placed in the package context.
