#!/usr/bin/env python
from sqlalchemy_config import config as cfg
from sqlalchemy import and_
from umls_tools import orm as o
from py2neo import Graph
from tqdm import tqdm
import pandas as pd
import os
import csv
import re
# import sys
# import pprint as pp


home = os.environ['HOME']
icd_file = os.path.join(home, "code/disease_atlas/phecodes/build/phe_map.csv")
mesh_file = os.path.join(home, "Downloads/all_filtered_trials.csv")
outfile = os.path.join(home, "neo4j_mesh_map.txt")

config_file = os.path.join(home, ".db.cnf")
config_section = 'umls_2022aa_mysql'
prefix = 'umls.'

# Connect to graph 7687 if locally on dtadb2, otherwise 7688
umlsg = Graph("bolt://localhost:7687", auth=None)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MapResult(object):
    """Container class for the mapping results.
    """
    ATTRS = ['valid_mesh_code', 'valid_mesh_term', 'icd_code',
             'icd_term', 'sab', 'type', 'path_len']
    HEADER = ATTRS + ['atlas_icd10', 'phecode']

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, mesh_code=None, valid_mesh_term=None, icd_code=None,
                 icd_term=None, atlas_map=None, path_len=None, rel_path=None,
                 sab=None, other_icd=None, type=None, provided_mesh_term=None):
        self.valid_mesh_code = mesh_code
        self.valid_mesh_term = valid_mesh_term
        self.provided_mesh_term = provided_mesh_term
        self.icd_code = icd_code
        self.icd_term = icd_term
        self.atlas_map = atlas_map
        self.path_len = path_len
        self.rel_path = rel_path
        self.sab = sab
        self.other_icd = other_icd
        self.type = type

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def dict(self):
        """Turn all the attributes into a dictionary (`dict`)
        """
        data = dict([(i, getattr(self, i)) for i in self.ATTRS])
        try:
            data['atlas_icd10'] = self.atlas_map[0]
            data['phecode'] = self.atlas_map[1]
            data['icd_term'] = "|".join(data['icd_term'])
        except TypeError:
            data['atlas_icd10'] = None
            data['phecode'] = None
            data['icd_term'] = None

        return data

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing
        """
        attr = []
        for i in self.ATTRS:
            attr.append(f"{i}='{getattr(self, i)}'")
        return f"<{self.__class__.__name__}({','.join(attr)})>"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache(inpath):
    """Cache the ATLAS results into a dictionary.

    Parameters
    ----------
    inpath : `str`
        The path to the ATLAS file.

    Returns
    -------
    cache : `dict`
        The Atlas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.
    """
    cache_dict = {}
    with open(inpath) as infile:
        reader = csv.reader(infile)
        next(reader)
        for row in reader:
            code = row[0]
            code = re.sub(r'\D+$', '', code)
            cache_dict[code] = row
    return cache_dict


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def validate_mesh(session, term, code):
    """Validate the MeSH code against the UMLS.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object to issue queries against the UMLS
        relational database.
    term : `str`
        The unvalidated MeSH term.
    code : `str`
        The unvalidated MeSH code (ID).

    Returns
    -------
    valid_term : `str`
        The validated MeSH term.
    valid_code : `str`
        The validated MeSH code (ID).

    Raises
    ------
    ValueError
        If the MeSH code/term can't be validated.

    Notes
    -----
    This performs the following steps.

    1. Query the MeSH code against the UMLS and look at the returned terms to
    see if they match the unvalidated term.
    2. If so then it is validated and returned. If not, then unvalidated the
    term from is  queried against MESH, if a single MeSH code is returned then
    it is validated and returned.
    3. If the code can't be uniquly Id'd. The unvalidated term is queried
    against all of the UMLS and compared against the CUI concepts of the
    possible terms retrieved in step 1, if there is a match then it is
    validated.
    """
    term = re.sub(r'^"|"$', '', term.strip())
    term = re.sub(r"^'|'$", '', term)
    sql = session.query(
        o.MrConso
    ).filter(
        and_(o.MrConso.CODE == code, o.MrConso.SAB == "MSH")
    )

    possible_terms = []
    for row in sql:
        if row.STR.lower() == term.lower():
            return term, code
        possible_terms.append((row.STR, row.CUI))

    # If we get here try a term search
    sql = session.query(
        o.MrConso
    ).filter(
        and_(o.MrConso.STR == term, o.MrConso.SAB == "MSH")
    )

    mesh_codes = set([])
    for row in sql:
        mesh_codes.add(row.CODE)

    if len(mesh_codes) == 1:
        return term, list(mesh_codes)[0]

    sql = session.query(
        o.MrConso
    ).filter(
        and_(o.MrConso.STR == term)
    )

    for row in sql:
        for t, c in possible_terms:
            if c == row.CUI:
                return t, code

    # If we get here, then there is no validation so we check the MeSH code is
    # ok
    if len(code) == 10:
        non_zero = re.match(r'([C/D])0*([1-9][0-9]*)$', code)
        lead = non_zero.group(1)
        val = non_zero.group(2)
        if len(val) < 7:
            val = f"{lead}{val.rjust(6, '0')}"
            return validate_mesh(session, term, val)
    raise ValueError(f"unknown MeSH code/term: {code}/{term}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def map_to_atlas(umlsg, mesh_code, atlas):
    """Perform the full mapping process. First direct, then indirect, then CUI.
    This is performed hierarchically and returns when the first matches are
    found.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    mesh_code : `str`
        A validated MeSH code.
    atlas : `dict`
        The Alas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, this will always have a length of 1 but a path_len
        of -1 if no mapping is found.
    """
    mapping = \
        direct_mapping(
            umlsg, mesh_code, atlas
        )

    if mapping.path_len == 1:
        return [mapping]
    mappings = remove_dups(indirect_mapping(umlsg, mesh_code, atlas))
    if len(mappings) == 0:
        mappings = remove_dups(cui_bounce(umlsg, mesh_code, atlas))

    if len(mappings) == 0:
        return [mapping]
    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def remove_dups(mappings):
    """Remove duplicated mappings, this is based on ICD10 code.

    Parameters
    ----------
    mappings : `list` of `MapResult`
        The mapping results.

    Returns
    -------
    unique_mappings : `list` of `MapResult`
        The unique mapping results.
    """
    uniques = []
    seen_icd = set([])
    for i in mappings:
        if i.icd_code not in seen_icd:
            uniques.append(i)
            seen_icd.add(i.icd_code)
    return uniques


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def direct_mapping(umlsg, mesh_code, atlas):
    """Attempt an direct mapping by looking for a single cui_mapping step.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    mesh_code : `str`
        A validated MeSH code.
    atlas : `dict`
        The Alas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, will be empty if nothing was found.
    """
    cql = """
    MATCH (n:Source {code: $code})-[r:cui_mapping]-(p)
    WHERE p:ICD10
    RETURN n.code, p.code, p.term, labels(p), r
    """
    r = umlsg.run(cql, parameters={'code': mesh_code})
    # print(r)
    icd_maps = []
    while r.forward():
        mesh_code, icd_code, icd_term, labels, rels = r.current
        labels.pop(labels.index("Source"))
        icd_label = labels[0]
        icd_test = re.sub(r'\.', '', icd_code)
        try:
            return MapResult(
                mesh_code=mesh_code, icd_code=icd_code,
                icd_term=icd_term, atlas_map=atlas[icd_test],
                path_len=1, rel_path=rels, sab=icd_label,
                other_icd=icd_maps, type="direct"
            )
        except KeyError:
            icd_maps.append((mesh_code, icd_code, icd_label))
    return MapResult(
        mesh_code=mesh_code, icd_code=None,
        icd_term=None, atlas_map=None,
        path_len=-1, rel_path=None, sab=None,
        other_icd=icd_maps, type="direct"
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def indirect_mapping(umlsg, mesh_code, atlas):
    """Attempt an indirect mapping by allowing for 0-1 parent/child steps both
    before and after a cui_mapping step.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    mesh_code : `str`
        A validated MeSH code.
    atlas : `dict`
        The Atlas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, will be empty if nothing was found.
    """
    # WHERE p.ICD10 OR p.ICD10CM
    cql = """
    MATCH l=(n:Source {code: $code})-[x:PAR|CHD*0..1]-()-[y:cui_mapping]-()-[z:PAR|CHD*0..1]-(p)
    WHERE p:ICD10
    RETURN n.code, p.code, p.term, labels(p), length(l), l
    """
    r = umlsg.run(cql, parameters={'code': mesh_code})

    icd_maps = []
    while r.forward():
        mesh_code, icd_code, icd_term, labels, length, rels = r.current
        labels.pop(labels.index("Source"))
        icd_label = labels[0]
        icd_test = re.sub(r'\.', '', icd_code)
        try:
            icd_maps.append(
                MapResult(
                    mesh_code=mesh_code, icd_code=icd_code,
                    icd_term=icd_term, atlas_map=atlas[icd_test],
                    path_len=length, rel_path=rels, sab=icd_label,
                    type="indirect"
                )
            )
        except KeyError:
            pass
    icd_maps.sort(key=lambda x: x.path_len)
    try:
        match_len = icd_maps[0].path_len
    except IndexError:
        return []

    return [i for i in icd_maps if i.path_len == match_len]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cui_bounce(umlsg, mesh_code, atlas):
    """Attempt a mapping by traversing the cui_mapping edges up to 3 times.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    mesh_code : `str`
        A validated MeSH code.
    atlas : `dict`
        The Alas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, will be empty if nothing was found.
    """
    cql = """
    MATCH l=(n:Source {code: $code})-[y:cui_mapping*0..3]-(p)
    WHERE p:ICD10
    RETURN n.code, p.code, p.term, labels(p), length(l), l
    """
    # print("*** CALLED ***")
    r = umlsg.run(cql, parameters={'code': mesh_code})

    icd_maps = []
    while r.forward():
        mesh_code, icd_code, icd_term, labels, length, rels = r.current
        labels.pop(labels.index("Source"))
        icd_label = labels[0]
        icd_test = re.sub(r'\.', '', icd_code)
        try:
            icd_maps.append(
                MapResult(
                    mesh_code=mesh_code, icd_code=icd_code,
                    icd_term=icd_term, atlas_map=atlas[icd_test],
                    path_len=length, rel_path=rels, sab=icd_label,
                    type="cui"
                )
            )
        except KeyError:
            pass
    icd_maps.sort(key=lambda x: x.path_len)
    try:
        match_len = icd_maps[0].path_len
    except IndexError:
        return []
    return [i for i in icd_maps if i.path_len == match_len]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def do_mapping(umlsg, writer, code, term, provided_term, provided_code, atlas,
               row_vals):
    """Perform the mapping process and write out the results.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    writer : `sv.DictWriter`
        The writer for writing to file.
    code : `str`
        A validated MeSH code.
    term : `str`
        A validated MeSH term.
    provided_term : `str`
        The MeSH term that was provided and validated. Might not be the same
        as ``term``.
    atlas : `dict`
        The Alas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.
    """
    mappings = map_to_atlas(
        umlsg, code, atlas
    )

    for i in mappings:
        for row in row_vals:
            outrow = {**row, **i.dict}
            outrow['condition_mesh_term'] = provided_term
            outrow['condition_mesh_id'] = provided_code
            i.valid_mesh_term = term
            writer.writerow({**outrow, **i.dict})


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_bad_row():
    """Return a bad (non-mapped) row for output.

    Parameters
    ----------
    provided_mesh_id : `str`
        The MeSH ID that could not be validated.
    provided_mesh_term : `str`
        The MeSH term that could not be validated.

    Returns
    -------
    bad_row : `dict`
        A bad row that is written to file.
    """
    return dict(
        valid_mesh_code=None,
        valid_mesh_term=None,
        icd_code=None,
        icd_term=None,
        sab=None,
        type="bad_row",
        path_len=-1
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_delimiter_field(text):
    """The input trial files have lists collapsed as text, this turns them back
    into proper lists.

    Parameters
    ----------
    text : `str`
        The collapsed list.

    Returns
    -------
    expanded_list : `list` of `str`
        The text represented as a list.
    """
    try:
        text = re.sub(r'^\[|\]$', '', text)
        text = re.sub(r"', '", "','", text)
        reader = csv.reader([text], quotechar="'")
        return next(reader)
    except TypeError:
        return [None]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_trials(mesh_file):
    """Cache all the trial data into a dictionary where the keys are tuples of
    mesh term/mesh code, and the values are lists of trial rows that have them.

    Parameters
    ----------
    mesh_file : `str`
        The path to the MeSH file. must have a condition_mesh_term and
        condition_mesh_id column.

    Returns
    -------
    mesh_cache : `dict`
        the MeSH term/code cache mapped to trial rows.
    """
    # Read all of them in, will use pandas as easy to remove dups
    md = pd.read_csv(mesh_file)
    trial_cache = dict()
    for i in md.itertuples(index=False):
        mesh_terms = process_delimiter_field(i.condition_mesh_term)
        mesh_ids = process_delimiter_field(i.condition_mesh_id)
        for t, c in zip(mesh_terms, mesh_ids):
            try:
                trial_cache[(t, c)].append(i._asdict())
            except KeyError:
                trial_cache[(t, c)] = [i._asdict()]
    return trial_cache, list(md.columns)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    sm = cfg.get_sessionmaker(
        config_section, prefix, config_arg=config_file
    )

    atlas = cache(icd_file)
    trial_cache, header = cache_trials(mesh_file)
    session = sm()

    with open(outfile, 'wt') as outcsv:
        writer = csv.DictWriter(outcsv, header + MapResult.HEADER,
                                delimiter="\t", extrasaction='ignore')
        writer.writeheader()
        idx = 0
        break_at = -100
        # Iterate through the dataframe
        for k, v in tqdm(trial_cache.items(), unit=" rows",
                         desc="[info] mapping mesh->ICD"):
            pterm, pcode = k
            # Skip completely empty rows
            if pterm is None and pcode is None:
                for row in v:
                    writer.writerow({**row, **get_bad_row()})
                continue

            try:
                # We validate the mesh code/and or term
                term, code = validate_mesh(
                    session, pterm, pcode
                )
                do_mapping(
                    umlsg, writer, code, term, pterm, pcode, atlas, v
                )
            except ValueError:
                # Bad code/term
                for row in v:
                    outrow = {**row, **get_bad_row()}
                    outrow['condition_mesh_term'] = pterm
                    outrow['condition_mesh_id'] = pcode
                    writer.writerow(outrow)
            except TypeError:
                # mesh code is nan (probably)
                try:
                    term, code = validate_mesh(session, pterm, None)
                    do_mapping(
                        umlsg, writer, code, term, pterm, pcode, atlas, v
                    )
                except Exception:
                    for row in v:
                        outrow = {**row, **get_bad_row()}
                        outrow['condition_mesh_term'] = pterm
                        outrow['condition_mesh_id'] = pcode
                        writer.writerow(outrow)
            if break_at == idx:
                break
            idx += 1
