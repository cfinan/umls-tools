#!/usr/bin/env python
"""Mapping the disease Atlas to MSH via the UMLS neo4j build
"""
from sqlalchemy_config import config as cfg
from umls_tools import orm as o
from sqlalchemy import and_
from py2neo import Graph
from tqdm import tqdm
import os
import csv
import re
import pprint as pp


home = os.environ['HOME']
icd_file = os.path.join(home, "code/disease_atlas/phecodes/build/phe_map.csv")
# mesh_file = os.path.join(home, "Downloads/all_filtered_trials.csv")
outfile_mesh = os.path.join(home, "neo4j_atlas_mesh_map.txt")

config_file = os.path.join(home, ".db.cnf")
config_section = 'umls_2022aa_mysql'
prefix = 'umls.'

# Connect to graph 7687 if locally on dtadb2, otherwise 7688
umlsg = Graph("bolt://localhost:7687", auth=None)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MapResult(object):
    """Container class for the mapping results.
    """
    ATTRS = ['valid_icd10', 'path_len', 'search_code', 'search_term', 'map_code',
             'map_term', 'map_sab', 'rel_path', 'type']
    HEADER = ['atlas_icd10', 'phecode'] + ATTRS

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, search_code=None, validated=False, search_term=None,
                 map_code=None, map_term=None, map_sab=None, path_len=None,
                 rel_path=None, type=None):
        self.search_code = search_code
        self.search_term = search_term
        self.valid_icd10 = validated
        self.map_code = map_code
        self.map_term = map_term
        self.map_sab = map_sab
        self.path_len = path_len
        self.rel_path = rel_path
        self.type = type

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def dict(self):
        """Turn all the attributes into a dictionary (`dict`)
        """
        return dict([(i, getattr(self, i)) for i in self.ATTRS])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing
        """
        attr = []
        for i in self.ATTRS:
            attr.append(f"{i}='{getattr(self, i)}'")
        return f"<{self.__class__.__name__}({','.join(attr)})>"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_atlas(inpath):
    """Cache the ATLAS results into a dictionary.

    Parameters
    ----------
    inpath : `str`
        The path to the ATLAS file.

    Returns
    -------
    cache : `dict`
        The Atlas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.
    """
    cache_dict = {}
    with open(inpath) as infile:
        reader = csv.reader(infile)
        next(reader)
        for row in reader:
            code = row[0]
            code = re.sub(r'\D+$', '', code)

            if len(code) > 3:
                code = code[:3] + "." + code[3:]
            cache_dict[code] = row
    return cache_dict


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def map_to_atlas(umlsg, search_code, map_to_sab):
    """Perform the full mapping process. First direct, then indirect, then CUI.
    This is performed hierarchically and returns when the first matches are
    found.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    search_code : `str`
        A The start node for the query.
    map_to_sab : `str`
        The source abbreviation that we want to find the shortest route to.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, this will always have a length of 1 but a path_len
        of None if no mapping is found.
    """
    mappings = remove_dups(direct_mapping(umlsg, search_code, map_to_sab))

    if len(mappings) == 0:
        mappings = remove_dups(
            cui_synonyms(umlsg, search_code, map_to_sab)
        )

    if len(mappings) == 0:
        mappings = remove_dups(
            indirect_mapping(umlsg, search_code, map_to_sab, dist=1)
        )

    if len(mappings) == 0:
        mappings = remove_dups(cui_bounce(umlsg, search_code, map_to_sab))

    if len(mappings) == 0:
        mappings = remove_dups(
            indirect_mapping(umlsg, search_code, map_to_sab, dist=2)
        )

    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def remove_dups(mappings):
    """Remove duplicated mappings, this is based on ICD10 code.

    Parameters
    ----------
    mappings : `list` of `MapResult`
        The mapping results.

    Returns
    -------
    unique_mappings : `list` of `MapResult`
        The unique mapping results.
    """
    uniques = []
    seen_map = set([])
    for i in mappings:
        if i.map_code not in seen_map:
            uniques.append(i)
            seen_map.add(i.map_code)
    return uniques


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def direct_mapping(umlsg, search_code, map_to_sab):
    """Attempt an direct mapping by looking for a single cui_mapping step.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    search_code : `str`
        A The start node for the query.
    map_to_sab : `str`
        The source abbreviation that we want to find the direct route to.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, will be empty if nothing was found.
    """
    cql = """
    MATCH (n:ICD10 {{code: $code}})-[r:cui_mapping]-(p)
    WHERE p:{0}
    RETURN n.code, n.term, p.code, p.term, labels(p), r
    """.format(map_to_sab)
    r = umlsg.run(cql, parameters={'code': search_code})
    # print(r)
    mappings = []
    while r.forward():
        search_code, search_term, map_code, map_term, labels, rels = r.current
        labels.pop(labels.index("Source"))
        rel_str = (
            f"(ICD10:{search_code})-[cui_mapping]"
            f"-(MSH:{map_code})"
        )
        mappings.append(
            MapResult(search_code=search_code, search_term=search_term,
                      validated=True, map_code=map_code, map_term=map_term,
                      map_sab=map_to_sab, path_len=1, rel_path=rel_str,
                      type="direct")
        )
    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def indirect_mapping(umlsg, search_code, map_to_sab, dist=1):
    """Attempt an indirect mapping by allowing for 0-1 parent/child steps both
    before and after a cui_mapping step.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    search_code : `str`
        A The start node for the query.
    map_to_sab : `str`
        The source abbreviation that we want to find the shortest route to.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, will be empty if nothing was found.
    """
    # WHERE p.ICD10 OR p.ICD10CM
    cql = """
    MATCH l=(n1:ICD10 {{code: $code}})-[r1:PAR|CHD*0..{1}]-(n2)-[r2:cui_mapping]-(n3)-[r3:PAR|CHD*0..{1}]-(n4)
    WHERE n4:{0}
    RETURN n1, n2, n3, n4, r1, r2, r3, length(l)
    """.format(map_to_sab, dist)
    r = umlsg.run(cql, parameters={'code': search_code})

    mappings = []
    while r.forward():
        x = r.current
        source_node = x['n1']
        dest_node = x['n4']
        source_labels = list(source_node.labels)
        source_labels.pop(source_labels.index("Source"))
        rel_seq = get_rel_path(
            x['r1'],
            rel_seq=[(source_node['code'], "|".join(source_labels))]
        )
        rel_seq = get_rel_path(x['r2'], rel_seq=rel_seq)
        rel_seq = get_rel_path(x['r3'], rel_seq=rel_seq)
        for idx, i in enumerate(rel_seq):
            if isinstance(i, tuple):
                rel_seq[idx] = f"({i[1]}:{i[0]})"
        rel_seq = "".join(rel_seq)
        # labels.pop(labels.index("Source"))
        mappings.append(
            MapResult(search_code=source_node['code'],
                      search_term=source_node['term'],
                      validated=True, map_code=dest_node['code'],
                      map_term=dest_node['term'], map_sab=map_to_sab,
                      path_len=x['length(l)'], rel_path=rel_seq,
                      type="indirect")
        )
    mappings.sort(key=lambda x: x.path_len)
    try:
        match_len = mappings[0].path_len
    except IndexError:
        return []

    return [i for i in mappings if i.path_len == match_len]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_rel_path(r, rel_seq):
    if not isinstance(r, list):
        r = [r]
    index_code = rel_seq[-1][0]
    for i in r:
        rel_str = "|".join(i.types())
        codes = [n['code'] for n in i.nodes]
        nodes = []
        for n in i.nodes:
            labels = list(n.labels)
            labels.pop(labels.index("Source"))
            nodes.append((n['code'], "|".join(labels)))
        to_flip = bool(codes.index(index_code))
        if to_flip is True:
            index_code = codes[0]
            rel_seq.append('<-')
            rel_seq.append(f'[{rel_str}]')
            rel_seq.append('-')
            rel_seq.append(nodes[0])
        else:
            index_code = codes[1]
            rel_seq.append('-')
            rel_seq.append(f'[{rel_str}]')
            rel_seq.append('->')
            rel_seq.append(nodes[1])
    return rel_seq


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cui_synonyms(umlsg, search_code, map_to_sab):
    """Attempt an indirect mapping by allowing for 0-1 parent/child steps both
    before and after a cui_mapping step.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    search_code : `str`
        A The start node for the query.
    map_to_sab : `str`
        The source abbreviation that we want to find the shortest route to.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, will be empty if nothing was found.
    """
    # WHERE p.ICD10 OR p.ICD10CM
    cql = """
    MATCH l=(n1:ICD10 {{code: $code}})-[r1:has_cui]-(n2:CUI)-[r2:RQ|RL*1..3]-(n3:CUI)-[r3:has_cui]-(n4)
    WHERE n4:{0}
    RETURN n1, n2, r2, n3, n4, length(l)
    """.format(map_to_sab)
    r = umlsg.run(cql, parameters={'code': search_code})

    mappings = []
    while r.forward():
        x = r.current
        source_node = x['n1']
        source_code = source_node['code']
        cui1 = x['n2']
        cui1_code = cui1['code']
        cui2 = x['n3']
        dest_node = x['n4']
        link = x['r2']
        links = set()
        for i in link:
            links.update(i.types())
        rel_str = (
            f"(ICD10:{source_code})-[has_cui]-(CUI:{cui1_code})-"
            f"[{'|'.join(links)}]-(CUI:{cui2['code']})-[has_cui]-"
            f"(MSH:{dest_node['code']})"
        )
        mappings.append(
            MapResult(search_code=search_code, search_term=source_node['term'],
                      validated=True, map_code=dest_node['code'],
                      map_term=dest_node['term'],
                      map_sab=map_to_sab, path_len=x['length(l)'],
                      rel_path=rel_str,
                      type="cui_synonym")
        )
    mappings.sort(key=lambda x: x.path_len)
    try:
        match_len = mappings[0].path_len
    except IndexError:
        return []

    return [i for i in mappings if i.path_len == match_len]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cui_bounce(umlsg, search_code, map_to_sab):
    """Attempt a mapping by traversing the cui_mapping edges up to 3 times.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    search_code : `str`
        A The start node for the query.
    map_to_sab : `str`
        The source abbreviation that we want to find the shortest route to.

    Returns
    -------
    mappings : `list` of `MapResult`
        The mapping results, will be empty if nothing was found.
    """
    cql = """
    MATCH l=(n1:Source {{code: $code}})-[r1:cui_mapping*0..3]-(n2)
    WHERE n2:{0}
    RETURN n1, n2, r1, length(l)
    """.format(map_to_sab)
    # print("*** CALLED ***")
    r = umlsg.run(cql, parameters={'code': search_code})

    mappings = []
    while r.forward():
        x = r.current
        source_node = x['n1']
        dest_node = x['n2']
        source_labels = list(source_node.labels)
        source_labels.pop(source_labels.index("Source"))
        rel_seq = get_rel_path(
            x['r1'],
            rel_seq=[(source_node['code'], "|".join(source_labels))]
        )
        # rel_seq = get_rel_path(x['r2'], rel_seq=rel_seq)
        # rel_seq = get_rel_path(x['r3'], rel_seq=rel_seq)
        for idx, i in enumerate(rel_seq):
            if isinstance(i, tuple):
                rel_seq[idx] = f"({i[1]}:{i[0]})"
        rel_seq = "".join(rel_seq)
        # labels.pop(labels.index("Source"))
        mappings.append(
            MapResult(search_code=source_node['code'],
                      search_term=source_node['term'],
                      validated=True, map_code=dest_node['code'],
                      map_term=dest_node['term'], map_sab=map_to_sab,
                      path_len=x['length(l)'], rel_path=rel_seq,
                      type="cui_multiple")
        )
        # search_code, search_term, map_code, map_term, labels, length, rels = \
        #     r.current

        # labels.pop(labels.index("Source"))
        # mappings.append(
        #     MapResult(search_code=search_code, search_term=search_term,
        #               validated=True, map_code=map_code, map_term=map_term,
        #               map_sab=map_to_sab, path_len=length, rel_path=rels,
        #               type="cui")
        # )
    mappings.sort(key=lambda x: x.path_len)
    try:
        match_len = mappings[0].path_len
    except IndexError:
        return []

    return [i for i in mappings if i.path_len == match_len]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def validate_icd(session, search_code):
    """Make sure the ICD10 code is valid
    """
    sql = session.query(o.MrConso).filter(
        and_(o.MrConso.CODE == search_code, o.MrConso.SAB == "ICD10")
    )
    search_terms = []
    for i in sql:
        search_terms.append(i.STR)
    return search_terms


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def do_mapping(session, umlsg, search_code, map_to_sab):
    """Perform the mapping process and write out the results.

    Parameters
    ----------
    umlsg : `py2neo.Graph`
        The graph object for querying the database.
    writer : `sv.DictWriter`
        The writer for writing to file.
    code : `str`
        A validated MeSH code.
    term : `str`
        A validated MeSH term.
    provided_term : `str`
        The MeSH term that was provided and validated. Might not be the same
        as ``term``.
    atlas : `dict`
        The Alas cache. keys are processed ICD10 codes values are lists of
        unprocessed code and phecode.
    """
    mappings = []
    # First validate the ICD10 code.
    search_terms = validate_icd(session, search_code)
    if len(search_terms) > 0:
        mappings = map_to_atlas(umlsg, search_code, map_to_sab)

        if len(mappings) == 0:
            mappings = [MapResult(search_code=search_code, validated=True,
                                  search_term=search_terms)]
        for i in mappings:
            yield i
    else:
        yield MapResult(search_code=search_code, validated=False)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    sm = cfg.get_sessionmaker(
        config_section, prefix, config_arg=config_file
    )
    session = sm()

    atlas = read_atlas(icd_file)

    try:
        with open(outfile_mesh, 'wt') as outcsv:
            writer = csv.DictWriter(outcsv, MapResult.HEADER,
                                    delimiter="\t", extrasaction='ignore')
            writer.writeheader()
            idx = 0
            break_at = -50
            for k, v in tqdm(atlas.items(), unit=" rows",
                             desc="[info] mapping Atlas->MSH"):
                for m in do_mapping(session, umlsg, k, "MSH"):
                    try:
                        m.search_term = "|".join(set(m.search_term))
                        m.map_term = "|".join(set(m.map_term))
                    except TypeError:
                        pass

                    m = m.dict
                    m['valid_icd10'] = int(m['valid_icd10'])
                    m['atlas_icd10'] = v[0]
                    m['phecode'] = v[1]
                    # print(m)
                    writer.writerow(m)
                if break_at == idx:
                    break
                idx += 1
    finally:
        session.close()
        # umlsg.close()
