#!/bin/bash
# CREATE INDEX ON :Atom(term);
cypher-shell -u neo4j -p password <<EOF
CREATE TEXT INDEX cui_idx FOR (n:Concept) ON (n.code);
CREATE TEXT INDEX code_idx FOR (n:Source) ON (n.code);
EOF
