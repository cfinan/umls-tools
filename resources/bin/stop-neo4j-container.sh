#!/bin/bash
# usage stop-neo4j-container <container_name> <docker image>
PROG_NAME="=== stop-neo4j-container.sh ==="
USAGE=$(
    cat <<EOF
$PROG_NAME
Stop the Neo4j docker container. This does not use docker stop as it causes the container to exit with a code 137 and then it can never be restarted again without crashing. Instead, this docker executes the neo4j stop command that stops the container cleanly.

USAGE: $(basename $0) [flags]
EOF
     )

. shflags

DEFINE_string "container" "umls_neo4j" "The name of the docker container to use, must be running already" "c"

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

set -euo pipefail
. common-neo4j-func.sh

container_id=$(container_running "$FLAGS_container")

if [[ $container_id == "..." ]]; then
    echo "[error] container not running: $FLAGS_container"
    exit 1
fi

set +Euoe pipefail
docker exec "$FLAGS_container" /var/lib/neo4j/bin/neo4j stop
echo ""
