#!/bin/bash
PROG_NAME="=== create-neo4j-container.sh ==="
USAGE=$(
    cat <<EOF
$PROG_NAME
Download the docker image and create a new container from it. This assumes that a container of that name does not already exist. This will also setup mount points for the neo4j database, so requires the user to give a directory name in which to place the data.

USAGE: $(basename $0) [flags] <neo4j_data_dir>
EOF
     )

. shflags

DEFINE_string "container" "umls_neo4j" "The name of the docker container to use, must not exist already" "c"
DEFINE_string "image" "neo4j:latest" "The Neo4j docker image to use" "i"
DEFINE_integer "bolt-port" 7687 "The Neo4j bolt authentication port to use" "b"
DEFINE_integer "web-port" 7474 "The Neo4j web-interface port to use" "w"

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

set -euo pipefail
neo4j_dir="$1"
neo4j_dir="$(readlink -f "$neo4j_dir")"

# The container name is optional but a good idea to include it
container_name="$FLAGS_container"

# Make sure the data directory exists
if [[ ! -e "$neo4j_dir" ]] || [[ ! -d "$neo4j_dir" ]]; then
    echo "data directory does not exists or is not a directory: $neo4j_dir" 1>&2
    exit 1
fi

# The directories that will be mounted into neo4j
import_dir="${neo4j_dir}/import"
plugins_dir="${neo4j_dir}/plugins"
data_dir="${neo4j_dir}/data"
logs_dir="${neo4j_dir}/logs"
conf_dir="${neo4j_dir}/conf"

# Make sure the directories that will be mounted are available
mkdir -p "$import_dir"
mkdir -p "$plugins_dir"
mkdir -p "$data_dir"
mkdir -p "$logs_dir"

# We first create a scrificial docker to grab the conf directory
docker_id=$(docker run -p"$FLAGS_web_port":"$FLAGS_web_port" \
                   -p"$FLAGS_bolt_port":"$FLAGS_bolt_port" \
                   -d \
                   -v "$data_dir":/data \
                   -v "$logs_dir":/logs \
                   -v "$import_dir":/var/lib/neo4j/import \
                   -v "$plugins_dir":/plugins \
                   --user="$(id -u):$(id -g)" \
                   --env NEO4J_AUTH=none \
                   "$FLAGS_image")

docker cp "$docker_id":/var/lib/neo4j/conf "${neo4j_dir}"
docker stop $docker_id
docker rm $docker_id
chmod -R o-w "$conf_dir"

docker run \
       --name "$FLAGS_container" \
       -p"$FLAGS_web_port":"$FLAGS_web_port" \
       -p"$FLAGS_bolt_port":"$FLAGS_bolt_port" \
       -d \
       -v "$data_dir":/data \
       -v "$logs_dir":/logs \
       -v "$import_dir":/var/lib/neo4j/import \
       -v "$plugins_dir":/plugins \
       -v "$conf_dir":/var/lib/neo4j/conf \
       --user="$(id -u):$(id -g)" \
       --env NEO4J_AUTH=none \
       "$FLAGS_image"

# --env NEO4j_initial_dbms_default__database=umls \
# --env NEO4J_AUTH=neo4j/password \
# --env NEO4J_dbms_active__database=umls \

