#!/bin/bash
PROG_NAME="=== init-empty-umls-mysql.sh ==="
USAGE=$(
    cat <<EOF
$PROG_NAME
Create an empty MySQL/MariaDB database with the correct character set and collation
for the UMLS. Note that this assumes a password is set and it will be asked on the
command line.

USAGE: $(basename $0) [flags]
EOF
     )

. shflags

DEFINE_string "user" "umls_user" "The MySQL/MariaDB username" "u"
DEFINE_string "host" "127.0.0.1" "The MySQL/MariaDB hostname" "H"
DEFINE_string "port" "3306" "The MySQL/MariaDB port" "P"
DEFINE_string "database" "umls" "The MySQL/MariaDB UMLS database name" "D"

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

set -euo pipefail
echo "Creating database $FLAGS_database"
mysql -u"$FLAGS_user" -h"$FLAGS_host" -P"$FLAGS_port" -p -e"CREATE DATABASE IF NOT EXISTS $FLAGS_database CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
echo "*** END ***"
