#!/bin/bash
PROG_NAME="=== build-umls-neo4j.sh ==="
USAGE=$(
    cat <<EOF
$PROG_NAME
Run the entire UMLS Neo4j build process. This will generate the container and extract the data from the UMLS and load into a Neo4j database running inside the container. The container must not already exist. The data is extracted into a data subdirectory within <neo4j_data_dir>. If <neo4j_data_dir> does not exist then it is created. The Neo4j container is created with user permissions and no authentication.

 The <db_connection> parameter can specify a file (i.e. an SQLite database), or an SQLAlchemy connection URL. If You do not want to place connection strings on the command line (for security reasons), you can specify a section name (with the --config-section argument) in a connection file (.ini) that points to a section with the connection URL. The URL should be given with the key 'umls.url' i.e.

[umls_2022aa_mysql]
umls.url = mysql+pymysql://user:password@hostname:port/umls_2022aa?charset=utf8

The location of the config file can be given with the --config parameter.

Please note, depending on the number of SABs being extracted, this could take a while to run.

USAGE: $(basename $0) [flags] <neo4j_data_dir> <db_connection>

Example:
build-umls-neo4j.sh --neo4j-db-name "umls-2022aa" --config-section umls_2022aa_mysql --config ~/.db.cnf --sabs "RCD,MSH,SNOMEDCT_US,ICD10,ICD10CM,ICD10PCS,MDR,SNM2,SNMI" -v --container "umls_2022aa" /data/umls/neo4j/umls_2022aa
EOF
     )

. shflags

DEFINE_string "container" "umls_neo4j" "The name of the docker container to use, must not exist already" "c"
DEFINE_string "image" "neo4j:latest" "The Neo4j docker image to use" "i"
DEFINE_integer "bolt-port" 7687 "The Neo4j bolt authentication port to use" "b"
DEFINE_integer "web-port" 7474 "The Neo4j web-interface port to use" "w"
DEFINE_string "sabs" "" "The source vocabularies to extract for. This should be a comma separated list with no spaces, if not defined the this defaults to all available SABs" "s"
DEFINE_string "tmp" "" "The tmp directory location to use, if not defined this will use the system temp location" "T"
DEFINE_boolean "verbose" 1 "Output progress" "v"
DEFINE_string "config" "" "The location of the config file with the UMLS relational database parameters" "f"
DEFINE_string "config-section" "" "The section name in the config file where the UMLS connection details are stored" "e"
DEFINE_string "neo4j-db-name" "umls" "The name of the Neo4j graph DB, this should use dashes not underscored" "n"
FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# FOr compatibility with bh_init.sh
FLAGS_tmp_dir="$FLAGS_tmp"

# Get the data directory
neo4j_dir="$1"
neo4j_dir="$(readlink -f "$neo4j_dir")"

if [[ -z "$neo4j_dir" ]]; then
    echo "[error] neo4j data directory not set" 1>&2
fi

# Get the data directory
db_file="$2"

if [[ -z "$db_file" ]]; then
    db_file="..."
fi

. bh_init.sh
echo_prog_name "$0"
info_msg "running in: $RUN_DIR"
script_dir="$(dirname "$0")"
info_msg "script dir: $script_dir"
mkdir -p "$neo4j_dir"

info_msg "[info] creating docker container"

# Create the container
create-neo4j-container.sh --container "$FLAGS_container" \
                          --image "$FLAGS_image" \
                          --bolt-port "$FLAGS_bolt_port" \
                          --web-port "$FLAGS_web_port" \
                          "$neo4j_dir"

# # These are directories that will be created by the container build script
plugins_dir="$neo4j_dir"/plugins
import_dir="$neo4j_dir"/import
conf_dir="$neo4j_dir"/conf

# Copy the docker execution scripts to the plugins directory
cp "$script_dir"/load-umls-neo4j.sh "$plugins_dir"
cp "$script_dir"/create-neo4j-indexes.sh "$plugins_dir"

# Now create the command line args for the config extraction script
args=(-o "$import_dir" --tmpdir "$WORKING_DIR")

if [[ $VERBOSE -eq 0 ]]; then
    args+=(-vv)
fi

if [[ "$FLAGS_config" != "" ]]; then
    args+=(--config "$FLAGS_config")
fi

if [[ "$FLAGS_config_section" != "" ]]; then
    args+=(--config-section "$FLAGS_config_section")
fi

sabs=($(echo "$FLAGS_sabs" | tr ',' ' '))

if [[ "${#sabs[@]}" -eq 1 ]] && [[ "${sabs[0]}" == "" ]]; then
    info_msg "extracting data for all SABs"
else
    info_msg "extracting data for ${#sabs[@]} SABs: $FLAGS_sabs"
    args+=(--sabs "${sabs[@]}")
fi

if [[ "$db_file" != "..." ]]; then
    args+=("$db_file")
fi

# Run the extraction script
umls-neo4j-extract ${args[@]}

# Now we load the Neo4j data
docker start "$FLAGS_container"
docker exec "$FLAGS_container" /plugins/load-umls-neo4j.sh $FLAGS_neo4j_db_name

# Stop the container
unset_run_env
info_msg "stopping container..."
stop-neo4j-container.sh --container "$FLAGS_container"
set_run_env

# Sleep before running to give the container time to shut down
info_msg "waiting for container shutdown..."
sleep 30

info_msg "switching databases..."
# Set the default database in the config file.
switch-neo4j-database.sh --container "$FLAGS_container" "$FLAGS_neo4j_db_name" "$conf_dir"

info_msg "don't forget to create node indexes!"

# END
