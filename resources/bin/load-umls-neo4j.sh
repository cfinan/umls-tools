#!/bin/bash
# usage load-umls-neo4j <db_name>
import_dir="/var/lib/neo4j/import"
db_name="${1:-umls}"

args=(
    --delimiter 'TAB'
    --trim-strings true
    --array-delimiter '|'
    --input-encoding "UTF-8"
)

# Add the node file arguments
files=($import_dir/*_nodes.txt)
for i in "${files[@]}"; do
    echo $i
    args+=(--nodes "$i")
done

# Add the relationship file arguments
files=($import_dir/*_rel.txt)
for i in "${files[@]}"; do
    echo $i
    args+=(--relationships "$i")
done

# Now execute
/var/lib/neo4j/bin/neo4j-admin database import full "${args[@]}" "$db_name"
