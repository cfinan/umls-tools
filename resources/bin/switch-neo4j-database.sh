#!/bin/bash
# usage stop-neo4j-container <container_name> <docker image>
PROG_NAME="=== switch-neo4j-database.sh ==="
USAGE=$(
    cat <<EOF
$PROG_NAME
Switch the Neo4j database used by the docker container. This adjusts the default database in the config file (neo4j.conf). It will first stop the docker container (if needed), adjust the config and then restart again (if needed).

USAGE: $(basename $0) [flags] <new_db_name> <conf_dir>
EOF
     )

. shflags

DEFINE_string "container" "umls_neo4j" "The name of the docker container to use, must not exist already" "c"

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

. common-neo4j-func.sh

new_db_name="$1"

if [[ -z "$new_db_name" ]]; then
    echo "[error] no new DB name has been defined"
    exit 1
fi

# Make sure we can find the neo4j config file
conf_dir="$(readlink -f "$2")"
conf_file="$conf_dir"/neo4j.conf

if [[ ! -e "$conf_file" ]]; then
    echo "[error] can't locate neo4j config file: $conf_file"
    exit 1
fi

set -euo pipefail

container_id=$(container_running "$FLAGS_container")

restart=0
if [[ $container_id != "..." ]]; then
    stop-neo4j-container.sh --container "$FLAGS_container"
    restart=1
fi

sed -i '/\s*#*\s*initial\.dbms\.default_database/d' "$conf_file"
echo "initial.dbms.default_database=$new_db_name" >> "$conf_file"

if [[ $restart -eq 1 ]]; then
    docker start "$FLAGS_container"
fi
