# Common functions

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Get the container ID of the container name if the container is running.
#
# Globals:
#   None
# Arguments:
#   The container name.
# Outputs:
#   The container ID if the container is running, or "..." if the container is
#   not running.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
container_running() {
    local container_name="$1"
    container_id=$(docker ps -f name="$container_name" -q)
    if [[ -z $container_id ]]; then
        container_id="..."
    fi
    echo $container_id
}
