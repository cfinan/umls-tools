#!/bin/bash
set -euo pipefail
PROG_NAME="=== connect-neo4j-container.sh ==="
USAGE=$(
    cat <<EOF
$PROG_NAME
Connect to an interactive terminal of the Neo4j docker container.

USAGE: $(basename $0) [flags]
EOF
     )

. shflags

DEFINE_string "container" "umls_neo4j" "The name of the docker container to use, will be started if not already" "c"

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

set -euo pipefail
. common-neo4j-func.sh

container_id=$(container_running "$FLAGS_container")

if [[ $container_id == "..." ]]; then
    docker start "$FLAGS_container"
fi

docker exec -it "$FLAGS_container" /bin/bash
