# Overview of `umls_copy`
The functions in this module allow the copying of a pre-built UMLS MySQL database. Into other databases. It was designed with SQLite in mind, however, it is based on SQLAlchemy therefore it is possible to use other RDBMS, although only coing from MySQL to SQLite has been tested. There is also a command line program that can be used to perform the copy.

The copying process uses SQLAlchemy table reflection and dynamic building of SQLAlchemy ORM classes for each table. As a consequence, all tables must have a primary key column. Many tables in the UMLS do not have a primary key column that are required by the SQLAlchemy ORM classes. Therefore, for those tables with no primary_key, one is added as the first column. The name for the primary key column is `<TABLENAME>_ID`.

## Caveats
Given that it has only been tested against MySQL to SQLite it is unlikely to be fully generalise. Some dialect specific mapping has to be carried out to map MySQL `*_bin` collations to sqlite `NOCASE` collations. In addition, MySQL `DOUBLES` have to be mapped to `Float`.

# Usage
`umls_copy` can be used either via the API or the command line.

## cmd-line usage
When used via the command line, it is expected that RDBMS that require passwords are specified via a config file (see below). Others such as SQLite can be specified as a file path on the command line.
```
$ umls_copy --help
usage: umls_copy [-h] [-v] [-c COMMIT_EVERY] source_db target_db

Copy UMLS from one database to another

positional arguments:
  source_db             This is either a path to a config file with a section
                        heading in the config file detailing the connection
                        options to the source database, delimited from the
                        config file path with a | (pipe), or it is a path to
                        an SQLite database file.
  target_db             This is either a path to a config file with a section
                        heading in the config file detailing the connection
                        options to the target database, delimited from the
                        config file path with a | (pipe), or it is a path to
                        an SQLite database file

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         give more output and progress monitor
  -c COMMIT_EVERY, --commit-every COMMIT_EVERY
                        When copying data commit to the target database
                        --commit-every rows, default=100000
```

Here is an example that copies a MySQL source UMLS database to an SQLite target database. The MySQL source database has the connection parameters detailed in the config file `~/.ukbb.cnf` under a section header `[umls_db_conn]`, and example of this config file can be seen below, config file extensions are expected to be either `.ini`, `.cnf` or `.cfg`:
```
$ umls_copy -v "~/.ukbb.cnf|umls_db_conn" /home/user/umls_2020aa.db
```
 
## API usage
The code snippet below will copy the UMLS database froma MySQL source to an SQLite target
```
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import create_engine

# The source database, this must exist
source_engine = create_engine(
    'mysql+pymysql://user:password@127.0.0.1:3306/umls_2020aa'
)
source_sm = sessionmaker(bind=source_engine)

# The target database connection, in the case of SQLite it does not
# have to exist. For others it probably has to exist (although the
# tables will be created)
target_engine = create_engine('sqlite:////home/user/umls_2020aa.db)
target_sm = sessionmaker(bind=target_engine)

# verbose=True will output the rate of proress for each table copy, commit_every
# will buffer that many rows before committing to the target database. Making it
# larger will increase RAM usage
copy_db(source_sm, target_sm, verbose=True, commit_every=100000):
```

## config file
The config file is a feature of many of the scripts in `umls_tools`. An example is shown below. The default location should be at `~/.umls.cnf`.
```
# Make sure no one else can see this file but you
[umls_db_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# See below for info in connection URLS:
# https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
# Also, don't forget to escape any % that are in the URL (with a second %)
umls.url=mysql+pymysql://user:password@127.0.0.1:3306/umls_2020aa
```
