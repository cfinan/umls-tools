# Overview of `unique_cuis`
This produces a text file containing a set of unique UMLS concept IDs (CUIs). This can be loaded up into a umls database as a useful lookup table to provide access to standardised term names.

## UMLS database config file
To run the `unique_cuis` script, the UMLS database connection parameters need to be supplied in a secure way. Rather then suppling them directly on the commend line, they should be supplied in a configuration file. The script uses `SQLAlchemy` to interact with the database and the config file should have arguments that can be read into an SQLAlchemy connection. An example of a config file is shown below:

```
# Make sure no one else can see this file but you
[umls_db_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# See below for info in connection URLS:
# https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
# Also, don't forget to escape any % that are in the URL (with a second %)
umls.url=mysql+pymysql://user:password@127.0.0.1:3306/umls_2020aa
```

The relevant parts are the header name `[umls_db_conn]` and the connection url `umls.url`.

## Usage
The command line usage is shown below:
```
$ unique_cuis --help
usage: unique_cuis [-h] [--delimiter DELIMITER] [-v] [--norank]
                   [--config CONFIG]
                   umlsdb

Create a unique concept text file from the MRCONSO table in the umls.
Uniqueness is defined on several parameters to hopefully reflect the best text
based term for each CUI

positional arguments:
  umlsdb                A heading in a ini config file that will contain
                        SQLAlchemy connection style parameters

optional arguments:
  -h, --help            show this help message and exit
  --delimiter DELIMITER
                        the delimiter of the output file (default=<TAB>)
  -v, --verbose         Shall i print all sorts of status to the user?
  --norank              Use the non UMLS method for getting the unique CUIs?
  --config CONFIG       An alternative location of the config file to look for
                        the heading .my.cnf file default is HOME

problems email: c.finan@ucl.ac.uk
```

And an example run is shown below. Here you can see we have supplied the config header as a positional argument `umls_db_conn` and the path to the config file as an options `--config` argument.

```
$ unique_cuis -v --config ~/.umls.cnf umls_db_conn > ~/unique_cuis.txt
= Unique UMLS CUIs (umls_tools v0.1.0a1) =
[info]  config value: /home/rmjdcfi/.umls.cnf
[info]  delimiter value: 	
[info]  norank value: False
[info]  umlsdb value: umls_db_conn
[info]  verbose value: True
[start] 2020-07-17 09:44:35
[finish] 2020-07-17 09:51:19 [runtime] 0:06:44
```

## Importing into MySQL
If you are using MySQL or Maria db, there is a small bash script that can be used to generate a `unique_umls_cui` table. This has a full text search index applied to the `all_fields`. If using this script, please combine your UMLS configuration options into you `~/.my.cnf` (also referred to as the defaults-extra file). An example of two connection parameters in a `~/.my.cnf` is shown below:

```
[client_umls_2020aa_local]
port=3306
host=127.0.0.1
user=user
password=password
database=umls_2020aa

# Make sure no one else can see this file but you
[umls_db_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# See below for info in connection URLS:
# https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
# Also, don't forget to escape any % that are in the URL (with a second %)
umls.url=mysql+pymysql://user:password@127.0.0.1:3306/umls_2020aa
```

An example of running the script is shown below. The strange option order is necessary as the `umls_2020aa_local` is used by the mysql client on the command line and the remainder of the arguments are passed through to `unique_cuis`. 

```
./umls_tools/resources/scripts/mysql/create_mysql_unique_umls_cui.sh _umls_2020aa_local -v umls_db_conn
= Unique UMLS CUIs (umls_tools v0.1.0a1) =
[info]  config value: /home/rmjdcfi/.my.cnf
[info]  delimiter value: 	
[info]  norank value: False
[info]  umlsdb value: umls_db_conn
[info]  verbose value: True
[start] 2020-07-17 10:13:15
```
