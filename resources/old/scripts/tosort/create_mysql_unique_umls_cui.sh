#!/bin/bash
set -uo pipefail

# To write the data to be loaded
temp_file=$(mktemp)

# Will clean up on error exit or CTRL-C
cleanup() {
    if [[ -e "$temp_file" ]]; then
	rm -f "$temp_file"
    fi
}

exit_clean() {
    cleanup
    exit 0
}

exit_error() {
    cleanup
    exit 1
}

# Set cleanup traps
trap 'exit_clean' EXIT
trap 'exit_error' ERR SIGINT

OPTION_GROUP="$1"
shift
# The option group in ~/.my.cnf MUST be first argument

# Create the unique CUIs
../../umls/unique_cuis.py "$@" > "$temp_file"

mysql --defaults-extra-file="~/.my.cnf" --defaults-group-suffix="$OPTION_GROUP" <<EOF
# TODO: Should devolve this to the ORM
DROP TABLE IF EXISTS unique_umls_cui;
CREATE TABLE unique_umls_cui (
cui_id INTEGER NOT NULL PRIMARY KEY,
cui VARCHAR(8) NOT NULL,
term TEXT,
ncui INT,
probcui DOUBLE,
semantic_types TEXT,
all_terms TEXT
);

LOAD DATA LOCAL INFILE "$temp_file" INTO TABLE unique_umls_cui IGNORE 1 LINES;
CREATE FULLTEXT INDEX ftsearch ON unique_umls_cui(all_terms);
EOF
