# Documentation on How to setup Metamap
MetaMap can be downloaded locally from the link on the [home page](https://metamap.nlm.nih.gov/MetaMap.shtml), specifically [here](https://metamap.nlm.nih.gov/MainDownload.shtml). I have had some problems downloading with `wget`, so it seems to work best downloading with the browser, I have not tried `curl` though.
