#!/usr/bin/env python
################################################################################
# A basic template script. Will find it's location and load up some of the most#
# common imports. Will also set up argparse with some basic inputs             #
################################################################################
# TODO: Add the failures to the cache
# -*- coding: utf-8 -*-
from __future__ import print_function

#import xlrd
import csv
import re
import pprint
import sys
import os
from operator import itemgetter

# Nicer way to use the command line arguments
import argparse

# Some of my misc functions
from pymisc import animate,gzopen,unicode,htools
from umls import similarityweb

# Version number
version=0.1

# Get the program name, this may be a symbolic link so get the
# full path to the program and them the path to the program directory
prog_name=sys.argv[0]
prog_full_path=""

# The program can be called in many different ways, via a link or directky called
# with a full or relative path, so first see if it is being called via a soft link
# If it is NOT a soft link then this will produce an OSError so we will then
# get the absolute path and go from there
try:
    prog_full_path=os.readlink(prog_name)
except OSError:
    prog_full_path=os.path.abspath(prog_name)

# Get the path to the program
prog_path=os.path.dirname(prog_full_path)

# Make a human readable program name, i.e. not the full path
prog_name=os.path.basename(prog_name)

# For debugging
pp = pprint.PrettyPrinter()

#varcache={}

################################################################################
############################# FUNCTIONS BELOW ##################################
################################################################################

def do_measure(term1,term2,args,umlssim):
    '''
    Call the appropriate measure and gather the results
    '''
    results={}
    if args.measure=="similarity":
        # We use the class defaults if the user has not supplied defaults. I am doing
        # it this way rather than using args parse as the defaults will differ 
        # depending on the measure
        results=umlssim.get_similarity(args.term1,
                                       args.term2,
                                       sab=args.sab or umlssim.sim_defaults['sab'],
                                       rel=args.rel or umlssim.sim_defaults['rel'],
                                       method=args.method or umlssim.sim_defaults['similarity'])
    elif args.measure=="relatedness":
        results=umlssim.get_relatedness(args.term1,
                                        args.term2,
                                        sab=args.sab or umlssim.rel_defaults['sab'],
                                        rel=args.rel or umlssim.rel_defaults['rel'],
                                        method=args.method or umlssim.rel_defaults['relatedness'])
    else:
        print("[error] unknown measure %s" % (args.measure),file=sys.stderr)
#    print(results)
    # Put the results in the order you want them output
    all_results=[]
    for i in results:
        all_results.append([i[k] for k in umlssim.results_keys])
    return all_results


################################################################################
#############################  FUNCTIONS END  ##################################
################################################################################

# Initialise the parser
parser = argparse.ArgumentParser(description='This get similarity measures between terms and/or CUIs from the UMLS. It uses the UMLS::Similarity web interface to do this')

# I am using strings instead of open or argparse.FileType('w') as the type. This
# allows a bit more flexibility
parser.add_argument('term1',type=str,nargs='?',metavar='[TERM|CUI]',
                    help='The first term (ignored if --infile is given)')
parser.add_argument('term2',type=str,nargs='?',metavar='[TERM|CUI]',
                    help='The second term (ignored if --infile is given)')
parser.add_argument('-s','--sab',type=str,help='The sources used to determine similarity or relatedness')
parser.add_argument('-r','--rel',type=str,help='The relationship types to determine similarity or relatedness')
parser.add_argument('-e','--method',
                    type=str,
                    help='The algorithms to measure similarity or relatedness')
parser.add_argument('-m','--measure',type=str,
                    default="similarity",
                    help='Do you want to measure similarity or relatedness')
parser.add_argument('-u','--url',type=str,
                    default='http://atlas.ahc.umn.edu',
                    help='The url to the UMLS similarity server')
parser.add_argument('-i','--infile',type=str,help='The path to the optional input file containing terms')
parser.add_argument('-o','--outfile',type=str,help='The path to the optional output file,default is to STDOUT')
parser.add_argument('-d','--delimiter',type=str,default="\t",help='The delimiter of the output file (default "\t"')
parser.add_argument('-q','--quote',type=str,default=None,help='The quoting of the output file (default no quoting)')
parser.add_argument('-v','--verbose',action="store_true",help='Do you want to print messages to STDOUT')
parser.add_argument('--debug',type=int,default=-1,help='For debugging stop processing after --debug lines in --infile (default -1)')
parser.add_argument('--perms',action="store_true",help='print all the allowed permutations of methods/sab/rel to STDOUT and exit')

# Parse the arguments
args=parser.parse_args()

# Create the umls similarity class with the correct server and vobsity arguments
umlssim=similarityweb.UmlsSimilarityWeb(server=args.url,verbose=args.verbose)

# Small helper class for selective printing, pritn to stderr to differentiate
# from program output to stdout
m=animate.Msg(args.verbose,prefix="",outfile=sys.stderr)

# Now specifically check the permutations allowed for the wen interface
# This is hard coded at the moment but there is no reason I can't connect 
# to the web and grab these from the drop down boxes
if args.perms:
    titles=['Allowed Similarity SAB:REL:',
            'Allowed Similarity Methods ABV:NAME',
            'Allowed Relatedness SAB:REL:',
            'Allowed Relatedness Methods ABV:NAME']
    allowed=[umlssim.allowed_sim_sabrel,
             umlssim.allowed_sim_method,
             umlssim.allowed_rel_sabrel,
             umlssim.allowed_rel_method]

    for c,i in enumerate(allowed):
        print("%s\n%s" % (titles[c],"="*len(titles[c])))
        for k,v in i.items():
            newv=v
            if isinstance(v,list):
                newv=" | ".join(v)

            print("%-10s%s" %(k,newv))
        print("")

    # Also print this programs version
    print("Versions\n========")
    print("%-30s%s" % (prog_name,str(version)))

    # Also print the version info from the UMLS similarity web-interface
    versions=umlssim.version()
    for k,v in versions.items():
          print("%-30s%s" % (k,v))
    
    sys.exit(1)

# If we have an input file then we want to open it to parse the contents for 
# the similarity measures
nlines=1
if args.infile:
    try:
        fh=open(args.infile, 'rb')

        # Count the number of lines in the file, can be used in the animator if it
        # doesn't take too long. If debugging we will use that value
        nlines=args.debug
        if args.debug<0:
            nlines=sum(1 for i in fh)
            fh.seek(0)
    except IOError as e:
        print('[fatal] can not open file: %s' % e,file=sys.stderr)
        sys.exit(1)
elif not args.term1 and not args.term2:
    print('[fatal] you must supply an --infile or two terms',file=sys.stderr)
    sys.exit(1)
    
# Give the user some infomation if they want it
m.msg("= %s v%s =" % (prog_name,str(version))) 
m.prefix='[info]'
m.msg("Term 1:  %s" % (args.term1))
m.msg("Term 2:  %s" % (args.term2))
m.msg("input file:  %s (%i lines)" % (args.infile,nlines))
m.msg("output file:  %s" % (args.outfile))
m.msg("delimiter:  %s" % (args.delimiter))
m.msg("quote:  %s" % (args.quote))
m.msg("verbose:  %s" % (args.verbose))
m.msg("debug lines:  %i" % (args.debug))
m.msg("sab:  %s" % (args.sab))
m.msg("rel:  %s" % (args.rel))
m.msg("measure:  %s" % (args.measure))
m.msg("method:  %s" % (args.method))
m.msg("url:  %s" % (args.url))


# Sort out the csv key word arguments according to what the user wants
csvkwargs={'delimiter': args.delimiter}
if args.quote==None:
    csvkwargs['quoting']=csv.QUOTE_NONE
else:
    csvkwargs['quotechar']=args.quote

# If we have an output file then open it and write the header
ofh=sys.stdout
if args.outfile:
    ofh=open(args.outfile,'w')

# Create a writer and write the header
writer=csv.writer(ofh,**csvkwargs)
writer.writerow(umlssim.results_keys)

# # Get a csv reader and the header then get the index
# # of the gwas catalogue variant columna and die if it
# # can't be found
if args.infile:
    with open(args.infile, 'rb') as csvfile:
        reader=csv.reader(csvfile,**csvkwargs)
        
        # This animates a progress line
        ani=animate.LineAnimate(nlines)
#        ani.next_line()

        # Loop throug all the rows
        for rn,row in enumerate(reader):
            ani.animate(message="Processing %i (%s vs %s)" % (rn,row[0],row[1]))
            ani.next_line()

            # Do the similarity/relatedness measure
            data=do_measure(row[0],row[1],args,umlssim)
            for i in data:
                writer.writerow(i)
            if rn==args.debug:
                break
            pass
else:
    # Do the similarity/relatedness measure
    data=do_measure(args.term1,args.term2,args,umlssim)
    for i in data:
        writer.writerow(i)

ofh.close()
m.prefix="\n"
m.msg("=== END ===")

