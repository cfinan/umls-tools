#!/usr/bin/env python
from __future__ import print_function
from py2neo import Graph
from umls import umls_neo4j

import pprint
pp = pprint.PrettyPrinter()

# Connect to graph 7687 if locally on dtadb2, otherwise 7688
umls_conn = Graph("bolt://localhost:7687", auth=("neo4j", "umls_2017ab"))

# Localise to these ontologies
sabs = ['MSH', 'ICD10', 'SNOMEDCT_US']
sab_str = ",".join(sabs)
umlsi = umls_neo4j.UmlsInterface(umls_conn,
                                 filter_longest_term=True,
                                 sab=sabs)

cui1 = "C0011860"
dist = None

print("* Children of '{}' within start='{}'"
      " and end={} and SAB='{}'".format(cui1,
                                        dist,
                                        dist,
                                        sab_str))
for i in umlsi.get_child_concepts(cui1, dist_start=dist, dist_end=dist):
    print("parent '{}', child '{}', dist='{}'".format(i[0]['cui'],
                                                      i[2]['cui'],
                                                      len(i[1])))

# A second CUI to test some measures
cui2 = "C0025517"

cuis = [i['cui'] for i in umlsi.get_shortest_path(cui1, cui2)]
print("* Shortest node path between "
      "'{}' and '{} = '{}'".format(cui1,
                                   cui2,
                                   ",".join(cuis)))

path_len = umlsi.get_shortest_path_len(cui1, cui2)
print("* Shortest node path length between "
      "'{}' and '{} = '{}'".format(cui1,
                                   cui2,
                                   path_len))


# Create a similarity object
umlssim = umls_neo4j.UmlsSimilarity(umlsi)

path_sim = umlssim.get_path_sim(cui1, cui2)
print("* Similarity based on 'path' between "
      "'{}' and '{} = '{}'".format(cui1,
                                   cui2,
                                   path_sim))

print("* Root concepts for the SABS '{}'".format(sab_str))
#for i in umlsi.get_root_concepts():
    # print("root '{}' ({})".format(i['cui'], ",".join(i['term'])))


print("* LCS for concepts '{}', '{}'".format(cui1, cui2))
umlsi.get_lcs(cui1, cui2)

scores = umlssim.get_wup_sim(cui1, cui2)
pp.pprint(scores)

scores = umlssim.get_wup_sim(cui1, cui1)
pp.pprint(scores)
