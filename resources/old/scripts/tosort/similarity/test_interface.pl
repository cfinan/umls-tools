use strict;
use warnings;
use Data::Dumper qw(Dumper);

use UMLS::Interface;
my $umls = UMLS::Interface->new({"driver" => "mysql",
				 "database" => "umls_2016ab",
				 "username" => "root",
				 "password" => "muppet",
				 "hostname" => "127.0.0.1",
				 "socket" => '',
				 "config" => "config_file.txt"});

# returns the table names in both human readable and hex form
my $hash = $umls->returnTableNames();
print Dumper $hash;
# foreach my $table (sort keys %{$hash}) { 
#     print "$table\n"; 
# }

# returns the root
my $root = $umls->root();
print "The root is: $root\n";

# returns the version of the UMLS currently being used
my $version = $umls->version();
print "The version of the UMLS is: $version\n";

# Get the configuration parameters
$hash = $umls->getConfigParameters;
print "The configuration parameters are: \n";
print Dumper $hash;

# function to check if a concept ID exists in the database.
my $concept = "C0018563";
if($umls->exists($concept)) {
    print "$concept exists\n";
}


# function that returns a list of concepts (@concepts) related
# to a concept $concept through a relation $rel
my $concept = "C0018563";
my $rel     = "SIB";
my $array   = $umls->getRelated($concept, $rel);
print "The concepts related to $concept using the $rel relation are: \n";
print Dumper $array;

$concept = "C0018563";
$array   = $umls->getTermList($concept);
print "The terms associated with $concept are:\n";
print Dumper $array;

$concept = "C0018563";
$array   = $umls->getDefTermList($concept);
print "The terms associated with $concept are:\n";
print Dumper $array;

$concept = "C0018563";
$array   = $umls->getAllTerms($concept);
print "The terms associated with $concept are:\n";
print Dumper $array;

# function returns all the compounds in the sources
# specified in the configuration file
# $hash = $umls->getCompounds();
# print Dumper $hash;

# my $sab   = "MSH";
# $array = $umls->getCuisFromSource($sab);
# foreach my $concept (@{$array}) {
#     print "$concept\n";
# }

# returns the relations and its source between two concepts
# my $concept1  = "C0018563";
# my $concept2  = "C0016129";
# my $array     = $umls->getRelationsBetweenCuis($concept1,$concept2);
# print "The relations between $concept1 and $concept2 are:\n";
foreach my $relation (@{$array}) { print "  $relation\n"; }

# $concept = "C0018563";
# $hash =  $umls->findDescendents($concept);
# print Dumper $hash;
$umls->setRealtimeOption(1);
my $concept1  = "C0018563";
my $concept2  = "C0016129";
$array     = $umls->findShortestPath($concept1,$concept2);
print "The shortest path(s) between $concept1 than $concept2 are:\n";
print Dumper $array;


$array     = $umls->findLeastCommonSubsumer($concept1,$concept2);
print "The LCS(es) between $concept1 than $concept2 iare:\n";
print Dumper $array;

$concept  = "C0018563";
my $double   = $umls->getIC($concept);
print "The IC of $concept is $double\n";

$concept  = "C0018563";
$double   = $umls->getSecoIntrinsicIC($concept);
print "The Intrinsic IC of $concept is $double\n";

$concept  = "C0018563";
$double   = $umls->getSanchezIntrinsicIC($concept);
print "The Intrinsic IC of $concept is $double\n";

# $concept  = "C0018563";
# $double   = $umls->getProbability($concept);
# print "The probability of $concept is $double\n";

# my $int = $umls->getN();
# print "The number of CUIs is $int\n";

# $concept  = "C0018563";
# $double   = $umls->getFrequency($concept);
# print "The frequency of $concept is $double\n";

# $hash = $umls->getPropagationCuis();

