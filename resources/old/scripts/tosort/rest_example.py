#!/usr/bin/env python
from umls import umls_rest
import os
import pprint as pp


query_str = "fracture of carpal bone"
rc = umls_rest.UmlsRestQuery(username=os.environ['UMLS_USER'],
                             password=os.environ['UMLS_PW'])
for i in rc.search(query_str):
    print(i)
