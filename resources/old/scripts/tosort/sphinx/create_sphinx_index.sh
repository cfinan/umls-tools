#!/bin/bash
################################################################################
# A couple of commands to create a sphinx index, this command should be run as #
# sudo                                                                         #
################################################################################

# Create the indexes
indexer umls_def_idx
indexer umls_idx
