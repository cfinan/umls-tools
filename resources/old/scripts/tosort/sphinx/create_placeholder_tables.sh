#!/bin/bash
################################################################################
# Script to create placeholder tables in UMLS to Allow SphinxSE queries of the #
# UMLS indexes from MySQL                                                      #
################################################################################

# EXAMPLE USAGE: ./create_placeholder_tables.sh root localhost 3306 umls_2016ab
USER="$1"
HOST="$2"
PORT="$3"
DB="$4"

echo "[info] Creating tables in $DB"

# Created using a here document
mysql -u"$USER" -p -h"$HOST" -P"$PORT" -D"$DB" <<EOF

CREATE TABLE umls_idx
(
id          BIGINT UNSIGNED NOT NULL,
weight      INTEGER NOT NULL,
query       VARCHAR(3071) NOT NULL,
cui         VARCHAR(50),
sab         VARCHAR(50),
INDEX(query)
) ENGINE=SPHINX CONNECTION="sphinx://127.0.0.1:9312/umls_idx";

CREATE TABLE umls_def_idx
(
id          BIGINT UNSIGNED NOT NULL,
weight      INTEGER NOT NULL,
query       VARCHAR(3071) NOT NULL,
cui         VARCHAR(50),
sab         VARCHAR(50),
INDEX(query)
) ENGINE=SPHINX CONNECTION="sphinx://127.0.0.1:9312/umls_def_idx";

EOF

