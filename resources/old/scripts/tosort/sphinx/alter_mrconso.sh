#!/bin/bash
################################################################################
# This alters the MRCONSO table in UMLS to add a AUTO_INCREMENT PRIMAY KEY     #
# column that will be used in Sphinx queries. The existing primary key 'AUI'   #
# is made a UNIQUE INDEX. So these changes should not alter the operation of   #
# MRCONSO                                                                      #
# This short script assumes that there is a password on the MySQL database     #
################################################################################

# The connection parameters to MySQL
USER="$1"
HOST="$2"
PORT="$3"
DB="$4"

echo "[info] Altering MRCONSO..."

# Issue the command, this may take a while to run
mysql -u"$USER" -p -h"$HOST" -P"$PORT" -D"$DB" -e"ALTER TABLE MRCONSO DROP PRIMARY KEY, ADD COLUMN id INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST, ADD UNIQUE INDEX X_MRCONSO_AUI(AUI);"
echo "*** END ***"
