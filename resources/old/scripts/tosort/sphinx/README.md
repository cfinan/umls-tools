# Create a full text index of UMLS in Sphinx #

This describes importing of UMLS MRCONSO table into a Sphinx. Sphinx is a full text searching database with similar functionality to MySQL. It is very quick and returns it's results by pagination. It is ideally suited for searching keywords against UMLS terms and is very easy to setup and query. Although, processing the results can be a bit fiddly. There full Sphinx manual can be found [here](http://sphinxsearch.com/docs/current.html). Note, that if you are just using Sphinx then you do not need to worry about the setup steps, just look at the queries.

## Installing Sphinx ##
Sphinx is a standalone database with a loose MySQL database. It can be used in two ways. Either, directly connecting and querying or querying via MySQL and making the results available in a placeholder table. This section describes how to install the Sphinx application. The process is described in detail [here](https://mariadb.com/kb/en/mariadb/installing-sphinx/). Here, Sphinx was being installed on a Mint 18 box.

Add the repository note that this is rel22 not rel21 as in the link above: 

    ppa:builds/sphinxsearch-rel22

Then update the repositories and install:

    sudo apt-get update
    sudo apt-get install sphinxsearch

## Enabling Sphinx in MySQL ##

This describes getting Sphinx up and running on MariaDB v10.1. It is based on [this](https://mariadb.com/kb/en/mariadb/about-sphinxse/). First enable the Sphinx plug-in in MySQL, login to MySQL as root and run:

    INSTALL SONAME 'ha_sphinx';

Sphinx should now be listed when the `SHOW ENGINES;` command is issued. To use sphinx directly in MySQL you need to do some further config, see [here](https://mariadb.com/kb/en/mariadb/configuring-sphinx/)

## Modifying MRCONSO ##

The procedure to build a Sphinx index involves making some minor modifications to MRCONSO. However, these should not impact anything else that may use MRCONSO:

1. Dropping the AUI as the **PRIMARY KEY** and making into a **UNIQUE KEY** index.
2. Creating a new **AUTO_INCREMENT PRIMARY KEY** column. This is done as Sphinx returns only integer keys for the columns it matches and these need to be mapped back to MRCONSO to get the actual values.

To make the changes to MRCONSO, run `alter_mrconso.sh`. This script assumes that the MySQL instance requires a password that will be asked for when the script is run. It can be run by:

    ./alter_mrconso.sh <USER> <HOST> <PORT> <DATABASE>

It may take a while to run and all the options are required.

## Building a Sphinx Index ##

Sphinx can build an index directly from a MySQL source. The MySQL source and other Sphinx options are defined in `/etc/sphinxsearch/sphinx.conf`. An example of the `sphinx.conf` is shown below.

To create a Sphinx index, you define sources and index settings separately. For clarity, in the config file below sources have a **src** suffix then the indexes have a **idx** suffix. The general Sphinx search options are under **searchd** and the indexe creation options are under **indexer** and the individual **_idx** entries.

Two sources were defined that provide slightly different data to the index. The source `umls_def_src` incorporates term description text and semantic types into the index. This may give a bit "metadata" to the index. However, it may make it to broad, I have not tested this yet. Therefore, the source `umls_idx` just indexes terms. Sphinx also provides the ability to include other data along side the index (that may or may not be included in the index). Columns that are defined as `sql_field_*` (where * can be int/string etc...) are included in the index **and** are available as data along side an index, this data can be used to filter/group results. `sql_attr_*` (where * can be int/string etc...) columns are similar to `sql_field_*` except they are not included in the index.

Note that in the source definitions in the config file below, the **sql_host** parameter has been set to 127.0.0.1 and not localhost. In Sphinx **rel21-r4761** on Mint 17.1 I had no problems with using localhost as the host definition. However, in **rel22** on Mint 18 the index could not connect to the MySQL instance when the host was localhost, it only worked when the host was changed to 127.0.0.1. On the Mint 18 machine MySQL was also listening across the LAN so this might have something to do with it. However, the MySQL connection to localhost was still fine on the command line MySQL client.


```
# This source is a MySQL instance that queries to incorporate, term
# descriptions 'def' and semantic types 'sty' into the index
source umls_def_src
{
	type			= mysql
	sql_host		= 127.0.0.1
#	sql_host		= localhost
	sql_user		= root
	sql_pass		= muppet
	sql_db			= umls_2016ab
	sql_port		= 3306	# optional, default is 3306
#	sql_sock = /var/lib/mysql/mysql.sock
	sql_query		= \
	SELECT m.id,\
	       m.cui,\
		   m.lui,\
		   m.sui,\
		   m.sab,\
		   m.code,\
		   m.str,\
		   GROUP_CONCAT(IFNULL(d.def,'')) AS def, \
		   GROUP_CONCAT(IFNULL(s.sty,'')) AS sty \
	FROM MRCONSO m \
	LEFT JOIN MRDEF d \
	ON d.aui = m.aui \
	LEFT JOIN MRSTY s \
	ON s.cui = m.cui \
	GROUP BY m.id

	sql_field_string		= cui
	sql_field_string		= sab
}

# This source is a MySQL instance that queries only the terms
source umls_src
{
	type			= mysql
	# 'localhost' doesn't seem to work
	# see: http://sphinxsearch.com/forum/view.html?id=511
	sql_host		= 127.0.0.1
	sql_user		= root
	sql_pass		= muppet
	sql_db			= umls_2016ab
	sql_port		= 3306	# optional, default is 3306

	sql_query		= \
	SELECT m.id,\
	       m.cui,\
		   m.lui,\
		   m.sui,\
		   m.sab,\
		   m.code,\
		   m.str \
	FROM MRCONSO m 

	sql_field_string		= cui
	sql_field_string		= sab
}

# Index from the UMLS definitions source
index umls_def_idx
{
	source			= umls_def_src
	path			= /var/lib/sphinxsearch/data/umls_def_idx
	docinfo			= extern
	charset_type		= sbcs
}

# Index from the UMLS term source
index umls_idx
{
	source			= umls_src
	path			= /var/lib/sphinxsearch/data/umls_idx
	docinfo			= extern
	charset_type		= sbcs
}

indexer
{
	mem_limit		= 512M
}

searchd
{
	listen			= 9312
	listen			= 9306:mysql41
	log			= /var/log/sphinxsearch/searchd.log
	query_log		= /var/log/sphinxsearch/query.log
	read_timeout		= 5
	max_children		= 30
	pid_file		= /var/run/searchd.pid
	seamless_rotate		= 1
	preopen_indexes		= 1
	unlink_old		= 1
	workers			= threads # for RT to work
	binlog_path		= /var/lib/sphinxsearch/data

	# max amount of matches the daemon ever keeps in RAM, per-index
	# WARNING, THERE'S ALSO PER-QUERY LIMIT, SEE SetLimits() API CALL
	# default is 1000 (just like Google)
	max_matches		= 350000
}
```
The Sphinx indexer is pretty fast and can be run using the indexer command. So to create the two indexes defined in the above config, the following command s can be used, along with the output:

```    
sudo indexer umls_def_idx
Sphinx 2.2.11-id64-release (95ae9a6)
Copyright (c) 2001-2016, Andrew Aksyonoff
Copyright (c) 2008-2016, Sphinx Technologies Inc (http://sphinxsearch.com)

using config file '/etc/sphinxsearch/sphinx.conf'...
WARNING: key 'charset_type' was permanently removed from Sphinx configuration. Refer to documentation for details.
WARNING: key 'charset_type' was permanently removed from Sphinx configuration. Refer to documentation for details.
WARNING: key 'max_matches' was permanently removed from Sphinx configuration. Refer to documentation for details.
indexing index 'umls_def_idx'...
collected 7764881 docs, 779.6 MB
sorted 109.0 Mhits, 100.0% done
total 7764881 docs, 779635959 bytes
total 215.377 sec, 3619856 bytes/sec, 36052.41 docs/sec
total 7765406 reads, 2.393 sec, 0.2 kb/call avg, 0.0 msec/call avg
total 4277 writes, 9.519 sec, 573.9 kb/call avg, 2.2 msec/call avg


sudo indexer umls_idx
Sphinx 2.2.11-id64-release (95ae9a6)
Copyright (c) 2001-2016, Andrew Aksyonoff
Copyright (c) 2008-2016, Sphinx Technologies Inc (http://sphinxsearch.com)

using config file '/etc/sphinxsearch/sphinx.conf'...
WARNING: key 'charset_type' was permanently removed from Sphinx configuration. Refer to documentation for details.
WARNING: key 'charset_type' was permanently removed from Sphinx configuration. Refer to documentation for details.
WARNING: key 'max_matches' was permanently removed from Sphinx configuration. Refer to documentation for details.
indexing index 'umls_idx'...
collected 7764881 docs, 568.8 MB
sorted 79.9 Mhits, 100.0% done
total 7764881 docs, 568794927 bytes
total 148.757 sec, 3823629 bytes/sec, 52198.12 docs/sec
total 7765401 reads, 2.442 sec, 0.2 kb/call avg, 0.0 msec/call avg
total 3980 writes, 3.963 sec, 545.4 kb/call avg, 0.9 msec/call avg
```

These commands have been placed in a bash script that can be run as superuser by:

    sudo ./create_sphinx_index.sh

A couple of warnings are generated by this command that will need to be sorted in due course:

```
WARNING: key 'charset_type' was permanently removed from Sphinx configuration. Refer to documentation for details.
WARNING: key 'charset_type' was permanently removed from Sphinx configuration. Refer to documentation for details.
WARNING: key 'max_matches' was permanently removed from Sphinx configuration. Refer to documentation for details.
```

## Starting the sphinxsearch service ##
Before using Sphinx the service must be started. On some versions installed from the repositories this seems to happen automatically, on other versions it doesn't. The sphinxsearch service can be started by:

    sudo service sphinxsearch start

Although even then some problems may occur, if that is the case then try the commands below to see if any error messages are apparent:

    sudo service sphinxsearch status
	sudo tail /var/log/syslog

Also, check that `START=yes` in `/etc/default/sphinxsearch`

## Connecting Directly to Sphinx ##

As detailed above, sphinx can be queried from within a MySQL instance (SphinxSE) or directly on a different port to MySQL (SphinxQL). To connect directly in the machine where Sphinx is installed (SphinxQL):

	mysql -h0 -P9306

this also seems to work:

    mysql -h127.0.0.1 -P9306

## Forwarding the Sphinx Port ##

If you are outside of UCL and want to access SphinxQL from your computer via a script or the command line MySQL client then localhost port 9306 can be forwarded to provide remote access to Sphinx (after you have connected to UCL through the **socrates** gateway). In Linux, in your `.ssh/config` add the line under localhost:

```
Host localhost
    Localforward 9306 127.0.0.1:9306
```

## Querying Sphinx Directly ##

SQL-like SELECT statements can be issued from a direct connection to Sphinx, see the [documentation](http://sphinxsearch.com/docs/current/sphinxql-select.html) for details. The query below illustrates a query for **type 2 diabetes**.


```
mysql> SELECT * FROM umls_def_idx WHERE MATCH('type 2 diabetes');
+---------+----------+-------------+
| id      | cui      | sab         |
+---------+----------+-------------+
| 4646959 | C2733147 | SNOMEDCT_US |
| 4092169 | C0011860 | MEDLINEPLUS |
| 1598500 | C0011860 | NCI         |
| 4549412 | C2919802 | SNOMEDCT_US |
| 4092167 | C3534592 | MEDLINEPLUS |
| 4093401 | C0020616 | MEDLINEPLUS |
| 4093398 | C0011849 | MEDLINEPLUS |
| 4093003 | C0085207 | MEDLINEPLUS |
| 5631445 | C0342276 | HPO         |
| 5636365 | C0085207 | HPO         |
| 7291828 | C0011860 | MSH         |
| 2914787 | C2984334 | NCI         |
| 3174832 | C3166091 | LNC         |
| 2211993 | C2874072 | ICD10CM     |
| 3198122 | C0293359 | MSH         |
|  885909 | C1869043 | MDR         |
|  656372 | C0872972 | NCI         |
| 1597343 | C0071097 | NCI         |
|   50350 | C0362046 | MSH         |
| 5683376 | C2974540 | MSH         |
+---------+----------+-------------+
20 rows in set (0.05 sec)

```

Note that the actual term text is not returned. For this, the id will need to be queried against the MRCONSO table in the UMLS MySQL instance. Also, by default Sphinx will return the top 20 matching results. This can be changed with the **LIMIT** clause as shown below.

```
mysql> SELECT * FROM umls_def_idx WHERE MATCH('type 2 diabetes') LIMIT 40;
+---------+----------+-------------+
| id      | cui      | sab         |
+---------+----------+-------------+
| 4646959 | C2733147 | SNOMEDCT_US |
| 4092169 | C0011860 | MEDLINEPLUS |
| 1598500 | C0011860 | NCI         |
| 4549412 | C2919802 | SNOMEDCT_US |
| 4092167 | C3534592 | MEDLINEPLUS |
| 4093401 | C0020616 | MEDLINEPLUS |
| 4093398 | C0011849 | MEDLINEPLUS |
| 4093003 | C0085207 | MEDLINEPLUS |
| 5631445 | C0342276 | HPO         |
| 5636365 | C0085207 | HPO         |
| 7291828 | C0011860 | MSH         |
| 2914787 | C2984334 | NCI         |
| 3174832 | C3166091 | LNC         |
| 2211993 | C2874072 | ICD10CM     |
| 3198122 | C0293359 | MSH         |
|  885909 | C1869043 | MDR         |
|  656372 | C0872972 | NCI         |
| 1597343 | C0071097 | NCI         |
|   50350 | C0362046 | MSH         |
| 5683376 | C2974540 | MSH         |
| 2897832 | C2984262 | NCI         |
| 4094844 | C0030286 | MEDLINEPLUS |
| 1929792 | C0541155 | NCI         |
| 5059348 | C3714619 | NCI         |
| 5690431 | C1667080 | MSH         |
| 5995832 | C0067385 | NCI         |
| 7612458 | C0389071 | MSH         |
| 3573352 | C3273872 | NCI         |
| 4722131 | C0362046 | MEDLINEPLUS |
|  651038 | C1704463 | NCI         |
| 2176153 | C2825818 | NCI         |
| 2177847 | C2825820 | NCI         |
| 2908634 | C2982484 | NCI         |
| 1930234 | C2697967 | NCI         |
| 1014219 | C1880879 | NCI         |
| 1937166 | C2698689 | NCI         |
| 2176152 | C2825817 | NCI         |
| 2187884 | C2825816 | NCI         |
| 2904062 | C2986675 | NCI         |
| 3171770 | C3171765 | LNC         |
+---------+----------+-------------+
40 rows in set (0.03 sec)
```

The limit clause will more results, however, there is an upper limit to the number of results returned. The **WEIGHT()** function can be used to give a score to the search as well:

```
mysql> SELECT *,WEIGHT() FROM umls_def_idx WHERE MATCH('type 2 diabetes') LIMIT 10;
+---------+----------+-------------+----------+
| id      | cui      | sab         | weight() |
+---------+----------+-------------+----------+
| 4646959 | C2733147 | SNOMEDCT_US |     6611 |
| 4092169 | C0011860 | MEDLINEPLUS |     5635 |
| 1598500 | C0011860 | NCI         |     5594 |
| 4549412 | C2919802 | SNOMEDCT_US |     5594 |
| 4092167 | C3534592 | MEDLINEPLUS |     4643 |
| 4093401 | C0020616 | MEDLINEPLUS |     4633 |
| 4093398 | C0011849 | MEDLINEPLUS |     4628 |
| 4093003 | C0085207 | MEDLINEPLUS |     4626 |
| 5631445 | C0342276 | HPO         |     4612 |
| 5636365 | C0085207 | HPO         |     4612 |
+---------+----------+-------------+----------+
10 rows in set (0.23 sec)
```

## Querying Sphinx from MySQL ##

Sphinx can also been queried from within MySQL, i.e. after connecting via localhost:3306 or localhost:3308 if the connection is via an ssh tunnel and that is what the MySQL port has been forwarded to. This is known as **[SphinxSE](http://sphinxsearch.com/docs/current.html#sphinxse)**To query from MySQL a placeholder table must be setup. See the section [Using SphinxSE](https://mariadb.com/kb/en/mariadb/about-sphinxse/) for more details. For the Sphinx indexes that have been created above the **CREATE TABLE** statements for the placeholder tables are:

```
CREATE TABLE umls_idx
(
id          BIGINT UNSIGNED NOT NULL,
weight      INTEGER NOT NULL,
query       VARCHAR(3071) NOT NULL,
cui         VARCHAR(50),
sab         VARCHAR(50),
INDEX(query)
) ENGINE=SPHINX CONNECTION="sphinx://127.0.0.1:9312/umls_idx";

CREATE TABLE umls_def_idx
(
id          BIGINT UNSIGNED NOT NULL,
weight      INTEGER NOT NULL,
query       VARCHAR(3071) NOT NULL,
cui         VARCHAR(50),
sab         VARCHAR(50),
INDEX(query)
) ENGINE=SPHINX CONNECTION="sphinx://127.0.0.1:9312/umls_def_idx";
```

There are a couple of things to note. Firstly the port is host and port is 127.0.0.1:9312. So when sphinx is queried from MySQL and not directly from Sphinx, the query is directed against the placeholder table and the results are output into the placeholder table.One advantage of querying within MySQL is that you can join the id column of the placeholder table directly to the MRCONSO UMLS table to get the UMLS terms that match the search. An example of the same `type 2 diabetes` search with MySQL is shown below:

```
SELECT u.*,m.str
FROM umls_def_idx u
JOIN MRCONSO m
ON m.id = u.id
WHERE query='Type 2 Diabetes;mode=extended';

+---------+--------+-------------------------------+----------+-------------+-------------------------------------------------------------+
| id      | weight | query                         | cui      | sab         | str                                                         |
+---------+--------+-------------------------------+----------+-------------+-------------------------------------------------------------+
| 4646959 |   6611 | Type 2 Diabetes;mode=extended | C2733147 | SNOMEDCT_US | Type 2 diabetes mellitus well controlled                    |
| 4092169 |   5635 | Type 2 Diabetes;mode=extended | C0011860 | MEDLINEPLUS | Diabetes Type 2                                             |
| 1598500 |   5594 | Type 2 Diabetes;mode=extended | C0011860 | NCI         | Type 2 Diabetes Mellitus                                    |
| 4549412 |   5594 | Type 2 Diabetes;mode=extended | C2919802 | SNOMEDCT_US | Brittle type 2 diabetes mellitus                            |
| 4092167 |   4643 | Type 2 Diabetes;mode=extended | C3534592 | MEDLINEPLUS | Diabetes in Children and Teens                              |
| 4093401 |   4633 | Type 2 Diabetes;mode=extended | C0020616 | MEDLINEPLUS | Diabetes Medicines                                          |
| 4093398 |   4628 | Type 2 Diabetes;mode=extended | C0011849 | MEDLINEPLUS | Diabetes                                                    |
| 4093003 |   4626 | Type 2 Diabetes;mode=extended | C0085207 | MEDLINEPLUS | Diabetes and Pregnancy                                      |
| 5631445 |   4612 | Type 2 Diabetes;mode=extended | C0342276 | HPO         | Maturity-onset diabetes of the young                        |
| 5636365 |   4612 | Type 2 Diabetes;mode=extended | C0085207 | HPO         | Maternal diabetes                                           |
| 7291828 |   4605 | Type 2 Diabetes;mode=extended | C0011860 | MSH         | Diabetes Mellitus, Type 2                                   |
| 2914787 |   4600 | Type 2 Diabetes;mode=extended | C2984334 | NCI         | Type I Diabetes Mellitus Pathway                            |
| 3174832 |   4587 | Type 2 Diabetes;mode=extended | C3166091 | LNC         | PhenX measure - personal history of type 1- type 2 diabetes |
| 2211993 |   4578 | Type 2 Diabetes;mode=extended | C2874072 | ICD10CM     | Type 2 diabetes mellitus with kidney complications          |
| 3198122 |   3614 | Type 2 Diabetes;mode=extended | C0293359 | MSH         | Insulin Lispro                                              |
|  885909 |   3612 | Type 2 Diabetes;mode=extended | C1869043 | MDR         | Hyperglycaemia/new onset diabetes mellitus (SMQ)            |
|  656372 |   3607 | Type 2 Diabetes;mode=extended | C0872972 | NCI         | Pioglitazone Hydrochloride                                  |
| 1597343 |   3607 | Type 2 Diabetes;mode=extended | C0071097 | NCI         | Pioglitazone                                                |
|   50350 |   3606 | Type 2 Diabetes;mode=extended | C0362046 | MSH         | Prediabetic State                                           |
| 5683376 |   3605 | Type 2 Diabetes;mode=extended | C2974540 | MSH         | Canagliflozin                                               |
+---------+--------+-------------------------------+----------+-------------+-------------------------------------------------------------+
```

The disadvantage of querying in this way is the query string is more fiddly. For more details on SphinxSE querying see [here](http://sphinxsearch.com/docs/current.html#sphinxse). Placeholder tables for `umls_def_idx` and `umls_idx` have been setup in the `umls_2016ab` database and can be queried in this way.

The create table commands have been placed in a script that can be called by:

    ./create_placeholder_tables.sh <USER> <HOST> <PORT> <DATABASE>

The user must have **CREATE TABLE** permissions in MySQL, for example:

    ./create_placeholder_tables.sh root localhost 3306 umls_2016ab

## Querying Sphinx from R and Python ##
I will eventually add some example code in the repository. However, you should be able to use `RMySQL` in R and `MySQLdb` or `pymysql` in python.

***** END *****
