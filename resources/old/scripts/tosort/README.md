# UMLS scripts
Some documentation of scripts that can be used in this UMLS repository. Note that not all the scripts are in this directory., these are some of the more general ones and some of the documentation for executable python modules that also allow programmatic access

## `unique_cuis.py`
This is an executable module file for creating a text file of unique UMLS concepts (CUIs). There are two methods available, my old method that used preferred terms, english terms, ts=P, suppressed terms, ontology priority and term length and the UMLS method that uses the MRRANK method (thanks to Magda for finding this!). You should use the UMLS MRRANK method, my method is in there just for reference. To use my method, run with the `--norank` flag.

The help for the script is below:
```
# Path relative to this directory
../../umls/unique_cuis.py --help
usage: unique_cuis.py [-h] [--delimiter DELIMITER] [-v] [--norank]
                      [--config CONFIG]
                      umlsdb

Create a unique concept text file from the MRCONSO tablein the umls.
Uniqueness is defined on several parameters to hopefully reflect the best text
based term for each CUI

positional arguments:
  umlsdb                A heading in a ini config file that will contain
                        SQLAlchemy connection style parameters

optional arguments:
  -h, --help            show this help message and exit
  --delimiter DELIMITER
                        the delimiter of the output file (default=<TAB>)
  -v, --verbose         Shall i print all sorts of status to the user?
  --norank              Use the non UMLS method for getting the unique CUIs?
  --config CONFIG       An alternative location of the config file to look for
                        the heading .my.cnf file default is HOME

problems email: c.finan@ucl.ac.uk
```

Note that this outputs to STDOUT, so should be redirected to a file. The fields it outputs are as follows:

1. `cui_id` - An integer index column
2. `cui` - The UMLS concept ID
3. `term` - The best AUI term that represents the CUI
4. `naui` - the number of AUIs in the CUI
5. `prob_cui` - The proportion of all AUIs that this CUI covers
6. `semantic_types` - A ` | ` (spaces are intentional) separated list of all semantic types for the CUI (unique and sorted)
7. `all_terms` - A ` | ` (spaces are intentional) separated list of all terms for the CUI (unique and sorted)

### Prerequisites
This requires `sqlalchemy`, `sqlalchemy-utils` and `numpy` to be installed. It also requires my `pyaddons` repository and `simple_progress`. There is a conda environment (called `umls`) in `conda_env/umls_conda_env.yml` that can be installed with:

```
conda env create --file conda_env/umls_conda_env.yml
```

or you can update your existing environment with:

```
# After activating your environment
conda env update --file conda_env/umls_conda_env.yml
```

You will also need to create a config `ini` file with the UMLS database connection parameters in it. The connection parameters should be in a format that SQL Alchemy understands, see [here](https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls) and [here](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config). As the file contains passwords, it should only be readable by you. An example is shown below:

```
[umls_conn]
# An example with SQLAlchemy connection URL, you do not have to use a URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# See below for info in connection URLS:
# https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
# Also, don't forget to escape any % that are in the URL (with a second %)
# prefix in entries must be 'umls.'
umls.url=mysql+pymysql://username:password@127.0.0.1:3310/umls_2018ab
```

### Examples
```
./unique_cuis.py --config "~/.my_other_config.cnf" -v umls_conn > ~/umls_unique_cui_rank.txt
==== Unique UMLS CUIs v0.1 ====
[info]  config value: /home/rmjdcfi/.gwas_catalog.cnf
[info]  delimiter value: 	
[info]  norank value: False
[info]  umlsdb value: umls_conn
[info]  verbose value: True
[start] 2019-12-13 16:03:13
[finish] 2019-12-13 16:20:26 [runtime] 0:17:13
```

## `create_mysql_unique_umls_cui.sh`
This is a wrapper around the `unique_cuis.py` script, that will generate the unique CUIs table (writing to temp) and then import it to a MySQL database. See above for documentation on `unique_cuis.py`.

The options you pass to the `unique_cuis.py` must be specified after an option group in your `.my.cnf` file (it will always look here). It will create a table called `unique_umls_cui` with a full text index on the `all_terms` field.

### Example of `~/.my.cnf` entry
The `~/.my.cnf` file should only be readable by you
```
[client_umls_2018ab_dtadb0]
user = my_username
password = my_password
database = umls_2018ab
host = 127.0.0.1
port = 3310
```

### Examples
```
./create_mysql_unique_umls_cui.sh '_umls_2018ab_dtadb0' --config "~/.my_other_config.cnf" -v umls_conn
```

__END__

