# The Specialist Lexicon #

This describes how to set up the UMLS specialist lexicon and basic usage. The specialist lexion is a set of Java-based tools and associatied data that provides lexial variants of various biomedical terms. It seems to be designed independently of the UMLS.

# Setup #
The data in the `LEX` directory contains the specialist lexicon data and the tools that were distribute with the `umls_2016ab`. The documentation is `HTML` and is located at `/LEX/DOCS/lvg/index.html`. There are several files and directories in the `LEX` root directory, however, all the install files are located in the `LEX/LEX_PGMS` directory. First, untar the file `lvg2016.tgz`.

    tar -xzvf lvg2016.tgz

This will produce a directory `lvg2016`. cd into that and there are two install scripts with the relevent one being `install_linux.sh`. This calls a script of the same name located in `lvg2016/install/bin`. The installation required a couple of small changes to the this install script, these are detailed below.

    LINE 13: FROM gtar -xzvf ./jreDist/jre-8u45-linux-x64.tgz TO: tar -xzvf ./jreDist/jre-8u45-linux-x64.tgz

    LINE 16: FROM tar -xzvf ./jreDist/jre-8u45-linux-i586.tgz TO: tar -xzvf ./jreDist/jre-8u45-linux-i586.tgz

The changes basically use tar instead of gtar, I have never heard of gtar and could not find it in the repositories but tar seemed to work fine. The documentation for the install can be found in `LEX/DOCS/lvg/docs/userDoc/install/install.html`.

The default installation uses the java based HSsql database. There are options for using MySQL but I decided to keep things simple as HSSql is the recommendation.

# Usage #
I don't want to re-iterate the manual. There are a couple of things to consider. The main program that will probably be used is lvg (lexical variant generator).

1. The options seem pretty complicated but are not actually that bad. For any given word you can generate inflections, synonyms spelling variants and derivational variants i.e. posy-, non- etc. These are controlled by input flows on the command line see `LEX/DOCS/lvg/docs/designDoc/UDF/flow/fs.html`. There are any other flow options as well but these are probably the most used.

2. The programs seem to expect input either via an input file, ask for input on the command line or input is piped into the program. so on the command line it will be something like:

    echo chemotherapy | ./lvg -ccgi -SC -SI -f:R -f:i -f:r -f:s -m

# Subterm Mapping Tool (STMT) #
I found this on the lexical tools website and it looks interesting. The home page for it is [here](https://lsg3.nlm.nih.gov/LexSysGroup/Projects/stmt/current/web/index.html). It is built to map terms/subterms and synonyms to the UMLS core. It looks and acts in a very similar way to the lexical variant generator

## Installation ##

    wget https://lsg3.nlm.nih.gov/LexSysGroup/Projects/stmt/current/release/stmt2015.tgz
	wget https://lsg3.nlm.nih.gov/LexSysGroup/Projects/stmt/current/release/HSqlDb.2016AB.tgz

I had some problems with `wget`, so `curl` seems to work:

```
curl --output HSqlDb.2017AB.tgz https://lsg3.nlm.nih.gov/LexSysGroup/Projects/stmt/current/release/HSqlDb.2017AB.tgz
curl --output stmt2015.tgz https://lsg3.nlm.nih.gov/LexSysGroup/Projects/stmt/current/release/stmt2015.tgz
```

The installation then proceeds as for the specialist lexicon above. To install the updated `HSqlDb.2016AB` database. The files were untared and copied to the `./STMT/stmt2015/data/HSqlDb` directory. The old databases were deleted. The database name (`DB_NAME`) was then changed from `stmt2015AA` to `stmt2016AB` in all of the following config files:

```
./STMT/stmt2015/data/Config/lsf.properties
./STMT/stmt2015/data/Config/smt.properties
./STMT/stmt2015/data/Config/stmt.properties
```

** __END__ **
