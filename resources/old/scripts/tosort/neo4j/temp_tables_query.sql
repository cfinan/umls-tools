## Estimating root nodes in the UMLS
USE umls_2016ab;

# Get SAB and SL
# Child relationships
CREATE TEMPORARY TABLE A AS
SELECT 		R.AUI1, R.SAB
FROM 		umls_2016ab.MRREL R
WHERE 		R.REL = "CHD"
GROUP BY    AUI1;

ALTER TABLE A ADD INDEX aaui_idx(aui1);

# parent relationships
CREATE TEMPORARY TABLE B AS
SELECT 		R.AUI1, R.SAB
FROM 		umls_2016ab.MRREL R
WHERE 		R.REL = "PAR"
GROUP BY    AUI1;

ALTER TABLE B ADD INDEX baui_idx(aui1);

# PAR/CHD roots
SELECT    M.CUI, A.SAB
FROM      A
LEFT JOIN B
ON 		  A.AUI1 = B.AUI1
JOIN      MRCONSO M
ON        M.AUI = A.AUI1
WHERE     B.AUI1 IS NULL
GROUP BY M.CUI;

# Now we do the same query with CUI and see what we get, I think
# some relationships are based on CUI -> CUI not CUI -> AUI -> AUI -> CUI
CREATE TEMPORARY TABLE C AS
SELECT 		R.CUI1, R.SAB
FROM 		umls_2016ab.MRREL R
WHERE 		R.REL = "CHD" AND CUI1 IS NOT NULL
GROUP BY    CUI1;

ALTER TABLE C ADD INDEX acui_idx(CUI1);

# parent relation ships
CREATE TEMPORARY TABLE D AS
SELECT 		R.CUI1, R.SAB
FROM 		umls_2016ab.MRREL R
WHERE 		R.REL = "PAR" AND CUI1 IS NOT NULL
GROUP BY    CUI1;

ALTER TABLE D ADD INDEX bcui_idx(CUI1);

# PAR/CHD roots, this gets the same as the AUI, so the CUI query does not give any more
SELECT    M.CUI, A.SAB
FROM      A
LEFT JOIN B
ON 		  A.AUI1 = B.AUI1
JOIN      MRCONSO M
ON        M.AUI = A.AUI1
WHERE     B.AUI1 IS NULL
UNION
SELECT    C.CUI1, C.SAB
FROM      C
LEFT JOIN D
ON 		  C.CUI1 = D.CUI1
WHERE     D.CUI1 IS NULL;


# Child relationships
CREATE TEMPORARY TABLE E AS
SELECT 		R.AUI1, R.SAB
FROM 		umls_2016ab.MRREL R
WHERE 		R.REL = "RN"
GROUP BY    AUI1;

ALTER TABLE E ADD INDEX eaui_idx(aui1);

# parent relationships
CREATE TEMPORARY TABLE F AS
SELECT 		R.AUI1, R.SAB
FROM 		umls_2016ab.MRREL R
WHERE 		R.REL = "RB"
GROUP BY    AUI1;

ALTER TABLE F ADD INDEX faui_idx(aui1);

# RB/RN roots, this doesn't seem to work in the same way
SELECT    M.CUI, F.SAB
FROM      F
LEFT JOIN E
ON 		  E.AUI1 = F.AUI1
JOIN      MRCONSO M
ON        M.AUI = F.AUI1
WHERE     E.AUI1 IS NULL
GROUP BY M.CUI;

