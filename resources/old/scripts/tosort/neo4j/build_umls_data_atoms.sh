#!/bin/bash
################################################################################
# Query out nodes and relationships to import into a Neo4j database            #
################################################################################

# Change for a different database
DB=umls_2016ab

# A complete list of AUI nodes with the term as a property as "Atom" and the
# Ontology as a label, restricted to English language terms
# The awk command adds quoting arounf the STR field, it also escapes the quotes
mysql -uroot -p -D$DB <<EOF | awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {gsub(/"/,"\\\"",$2);$2="\""$2"\"";}; print $0}' > ./data/aui_nodes.txt
SELECT AUI AS "aui:ID",
       STR AS term, CONCAT_WS(";",SAB,"Atom") AS ":LABEL" 
FROM   MRCONSO M
WHERE LAT = "ENG" 
GROUP BY AUI,STR;
EOF

# A complete list of CUI nodes, with "Concept" as a label, restricted to
# English language terms
mysql -uroot -p -D$DB <<EOF > ./data/cui_nodes.txt
SELECT   CUI AS "cui:ID",
         "Concept" AS ":LABEL" 
FROM     MRCONSO M
WHERE    LAT = "ENG" 
GROUP BY CUI;
EOF

# The relationships between CUI and AUI, restricted to English language terms
# TODO_DONE:
# TODO: isa, may be represented in the atom RELA relationships (see TODO below)
# TODO: so I will rename it CTA (ConceptToAtom)
mysql -uroot -p -D$DB <<EOF > ./data/cui_aui_rel.txt
SELECT   CUI AS ":START_ID", 
AUI      AS ":END_ID",
"CTA"    AS ":TYPE"
FROM     MRCONSO M
WHERE    LAT = "ENG" 
GROUP BY CUI,AUI;
EOF

# The relationships between the Atoms, each Atom in the relationship is back
# joined to MRCONSO so we can restrict to English language. Also, we ensure
# that "NULL" Atoms are not present. These are when the relationship is at
# the concept level as the MRREL table has relationships at various levels
# Not that REL is being used here not RELA, RELA is more detailed but is not
# available for every node, I could add additional relationships for RELA 
# types??
mysql -uroot -p -D$DB <<EOF > ./data/aui_aui_rel.txt
SELECT AUI1 AS ":START_ID",
       AUI2 AS ":END_ID",
       stype1 AS stype,
       stype2 AS etype,
       REL AS ":TYPE"
FROM   MRREL M
JOIN   MRCONSO C1
ON     C1.AUI = AUI1
JOIN   MRCONSO C2
ON     C2.AUI = AUI2
WHERE  AUI1 IS NOT NULL
AND    AUI2 IS NOT NULL
AND    AUI1 != AUI2 
AND    C1.LAT = "ENG"
AND    C2.LAT = "ENG";
EOF

# This adds all the RELA relationships that are available
# TODO_SCRAPPED: This is not a problem once the CUI->AUI relationship changed
# TODO_SCRAPPED: to CTA
# TODO: I think this can be improved, there are RELA's with the value "isa"
# TODO: and "reverse_isa", these are redundent when# we have a PAR/CHD
# TODO: relationship, so I will adjust the query to remove them in these cases
mysql -uroot -p -D$DB <<EOF > ./data/aui_aui_rela.txt
SELECT AUI1 AS ":START_ID",
       AUI2 AS ":END_ID",
       stype1 AS stype,
       stype2 AS etype,
       RELA AS ":TYPE"
FROM   MRREL M
JOIN   MRCONSO C1
ON     C1.AUI = AUI1
JOIN   MRCONSO C2
ON     C2.AUI = AUI2
WHERE  AUI1 IS NOT NULL
AND    AUI2 IS NOT NULL
AND    RELA IS NOT NULL
AND    AUI1 != AUI2 
AND    C1.LAT = "ENG"
AND    C2.LAT = "ENG";
EOF


echo "*** END ***"
