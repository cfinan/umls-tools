#!/bin/bash
################################################################################
# Query out PAR/CHD root nodes and SABs to import into a Neo4j database        #
################################################################################

# Change for a different UMLS database
DB="umls_2017ab"
UN="root"
PW=${MYSQLPW_ROOT}

# Create a table with unique CUIs to aid querys
mysql -q -u"$UN" -p"$PW" -D"$DB" <<EOF | awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {gsub(/"/,"\\\"",$2);$2="\""$2"\""; print $0}'  | body sort -k1,1 -t$'\t' > ./data/cui_par_chd_root_nodes.txt
# Get SAB and SL
# Child relationships
CREATE TEMPORARY TABLE A AS
SELECT 		R.AUI1, R.SAB
FROM 		MRREL R
WHERE 		R.REL = "CHD"
GROUP BY    AUI1;

ALTER TABLE A ADD INDEX aaui_idx(aui1);

# parent relationships
CREATE TEMPORARY TABLE B AS
SELECT 		R.AUI1, R.SAB
FROM 		MRREL R
WHERE 		R.REL = "PAR"
GROUP BY    AUI1;

ALTER TABLE B ADD INDEX baui_idx(aui1);

# PAR/CHD roots
SELECT    M.CUI, A.SAB
FROM      A
LEFT JOIN B
ON 		  A.AUI1 = B.AUI1
JOIN      MRCONSO M
ON        M.AUI = A.AUI1
WHERE     B.AUI1 IS NULL
GROUP BY M.CUI;
EOF

tmp_file=$(mktemp)
cat cui_nodes.txt | body sort -S"10G" -t$'\t' -k1,1 | join -11 -21 --header -t$'\t' -a1 - cui_par_chd_root_nodes.txt | awk 'BEGIN{FS="\t"; OFS="\t"}{if (NR > 1) {if ($8 != "") {$7=$7"|root_node|"$8; gsub(/"/,"",$7)}; print $1,$2,$3,$4,$5,$6,"\""$7"\""} else {print $1,$2,$3,$4,$5,$6,$7}}' > "$temp_file"

# TODO: Need to trap in pipefail
mv "$temp_file" cui_nodes.txt
