#!/bin/bash
sudo neo4j-import --array-delimiter '|' \
     --into /var/lib/neo4j/data/databases/umlsconcept_sty.db \
     --delimiter TAB \
     --input-encoding UTF-8 \
     --nodes /home/rmjdcfi/code/umls/neo4j/data/cui_nodes.txt \
     --nodes /home/rmjdcfi/code/umls/neo4j/data/sab_nodes.txt \
     --nodes /home/rmjdcfi/code/umls/neo4j/data/sty_nodes.txt \
     --relationships /home/rmjdcfi/code/umls/neo4j/data/cui_cui_rel.txt \
     --relationships /home/rmjdcfi/code/umls/neo4j/data/cui_cui_rela.txt \
     --relationships /home/rmjdcfi/code/umls/neo4j/data/cui_sab_isa.txt \
     --relationships /home/rmjdcfi/code/umls/neo4j/data/cui_sty_isa.txt \
     --relationships /home/rmjdcfi/code/umls/neo4j/data/sty_rel.txt


cypher-shell <<EOF
CREATE INDEX ON :Concept(cui);
CREATE INDEX ON :Concept(sab);
EOF
