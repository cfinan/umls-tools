#!/bin/bash
################################################################################
# Query out nodes and relationships to import into a Neo4j database            #
################################################################################

# Change for a different UMLS database
DB="umls_2017ab"
UN="root"
PW=${MYSQLPW_ROOT}

# Create a table with unique CUIs to aid querys
mysql -q -u"$UN" -p"$PW" -D"$DB" <<EOF
CREATE TABLE IF NOT EXISTS unique_cui AS (
SELECT  M.CUI,
        GROUP_CONCAT(DISTINCT STR ORDER BY STR SEPARATOR "|") AS STR, 
        GROUP_CONCAT(DISTINCT SAB ORDER BY SAB SEPARATOR "|") AS SAB,
        GROUP_CONCAT(DISTINCT TUI ORDER BY TUI SEPARATOR "|") AS TUI,
        t2.ncui,
        t2.pcui
FROM MRCONSO M 
JOIN MRSTY S
ON   M.CUI = S.CUI
JOIN 
(
SELECT t.*, LOG10(ncui / (SELECT count(*) AS total FROM MRCONSO)) * -1 AS pcui
FROM (
    SELECT CUI, count(*) AS ncui
    FROM MRCONSO
    GROUP BY CUI
    ) t 
) t2
ON       t2.CUI = M.CUI
GROUP BY M.CUI
);

ALTER TABLE unique_cui ADD PRIMARY KEY(CUI);
EOF


# # Use the unique cuis instead of commented out query above
# mysql -q -uroot -p -D$DB <<EOF | awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {gsub(/"/,"\\\"",$2);$2="\""$2"\"";}; print $0}' > ./data/cui_nodes_concepts.txt
# SELECT   CUI AS "cui:ID",
#          STR AS "term:string[]",
#          "Concept" AS ":LABEL"
# FROM     unique_cui 
# EOF
echo "CUI Nodes"
# Use the unique cuis instead of commented out query above
mysql -q -u"$UN" -p"$PW" -D"$DB" <<EOF | awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {gsub(/"/,"\\\"",$2);$2="\""$2"\""; gsub(/"/,"\\\"",$3);$3="\""$3"\""; gsub(/"/,"\\\"",$4);$4="\""$4"\"";}; print $0}' > ./data/cui_nodes.txt
SELECT   CUI AS "cui:ID",
         STR AS "term:string[]",
         SAB AS "sab:string[]",
         TUI AS "ui:string[]",
         ncui AS "ncui:int",
         pcui AS "pcui:double",
         "Concept" AS ":LABEL"
FROM     unique_cui 
EOF

# I will also place in some SAB and STN nodes. The SAB nodes will link to
# concepts, as will the STN. However, the STN will also have relationships
# with one another

# This gets all the SABs Nodes and their definitions. Note that is is limited
# to those SABs that are in MRCONSO
echo "SAB Nodes"
mysql -q -u"$UN" -p"$PW" -D"$DB" <<EOF | awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {gsub(/"/,"\\\"",$2);$2="\""$2"\"";}; print $0}' > ./data/sab_nodes.txt
SELECT   RSAB AS "sab:ID", 
         SSN AS "ssn:string", 
         "sab" AS ":LABEL"
FROM     MRSAB S
JOIN     MRCONSO M
ON       M.SAB = S.RSAB
GROUP BY RSAB
EOF

echo "STY Nodes"
# This gets all the semantic type nodes
mysql -q -u"$UN" -p"$PW" -D"$DB" <<EOF | awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {gsub(/"/,"\\\"",$2);$2="\""$2"\"";}; print $0}' > ./data/sty_nodes.txt
SELECT  ui AS "ui:ID",
        STY_RL AS "name:string",
        RT AS ":LABEL"
FROM    SRDEF;
EOF

# The relationships between the Atoms, each Atom in the relationship is back
# joined to MRCONSO so we can restrict to English language. Also, we ensure
# that "NULL" Atoms are not present. These are when the relationship is at
# the concept level as the MRREL table has relationships at various levels
# Not that REL is being used here not RELA, RELA is more detailed but is not
# available for every node, I could add additional relationships for RELA 
# types??
echo "CUI CUI Rel"
mysql -u"$UN" -p"$PW" -q -D"$DB" <<EOF > ./data/cui_cui_rel.txt
SELECT   CUI1 AS ":START_ID",
         CUI2 AS ":END_ID",
         REL AS ":TYPE"
FROM     MRREL M
JOIN     unique_cui C1
ON       C1.CUI = M.CUI1
JOIN     unique_cui C2
ON       C2.CUI = M.CUI2
WHERE    CUI1 != CUI2
GROUP BY CUI1, CUI2, REL
EOF

# # This adds all the RELA relationships that are available
echo "CUI CUI RelA"
mysql -u"$UN" -p"$PW" -q -D"$DB" <<EOF > ./data/cui_cui_rela.txt
SELECT   CUI1 AS ":START_ID",
         CUI2 AS ":END_ID",
         RELA AS ":TYPE"
FROM     MRREL M
JOIN     unique_cui C1
ON       C1.CUI = M.CUI1
JOIN     unique_cui C2
ON       C2.CUI = M.CUI2
WHERE    CUI1 != CUI2
AND      RELA IS NOT NULL
GROUP BY CUI1, CUI2, RELA
EOF

# This gets the retionships between the CUIs and SABs
echo "CUI SAB ISA"
mysql -u"$UN" -p"$PW" -q -D"$DB" <<EOF > ./data/cui_sab_isa.txt
SELECT   CUI AS ":START_ID",
         SAB AS ":END_ID",
         count(*) AS "ncui:int",
         "isa" AS ":TYPE"
FROM     MRCONSO
GROUP BY CUI, SAB
EOF

# This gets the retionships between the CUIs and STYs
echo "CUI STY ISA"
mysql -u"$UN" -p"$PW" -q -D"$DB" <<EOF > ./data/cui_sty_isa.txt
SELECT    M.CUI AS ":START_ID", 
          TUI AS ":END_ID", 
          "isa" AS ":TYPE" 
FROM      MRCONSO M 
JOIN      MRSTY S 
ON        M.CUI = S.CUI
GROUP BY  M.CUI, TUI
EOF

# This gets the retionships between the STYs
echo "STY REL Rel"
mysql -u"$UN" -p"$PW" -q -D"$DB" <<EOF > ./data/sty_rel.txt
SELECT      UI1 AS ":START_ID",
            UI2 AS ":END_ID",
            RL AS ":TYPE"
FROM (
SELECT   A.UI AS UI1,
         S.RL,
         B.UI AS UI2
FROM     SRSTR S
JOIN     SRDEF A
ON       A.STY_RL = S.STY_RL1
JOIN     SRDEF B
ON       B.STY_RL = S.STY_RL2
UNION
SELECT   S.UI1,
         C.STY_RL,
         S.UI2
FROM     SRSTRE1 S
JOIN     SRDEF C
ON       C.UI = S.UI2
) t2
WHERE    UI1 != UI2
GROUP BY UI1, RL, UI2
EOF


echo "*** END ***"
