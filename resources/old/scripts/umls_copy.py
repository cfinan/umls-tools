"""
.. include:: ../docs/umls_copy.md
"""
from umls_tools import __version__, __name__ as pkg_name
from simple_progress import progress
from pyaddons import sqlalchemy_helper
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import MetaData, Column, Integer, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mysql import DOUBLE
import argparse
import sys
import os
import re
import warnings
import pprint as pp

Base = declarative_base()

# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr
KNOWN_INI_EXT = ['.ini', '.cnf', '.cfg']

# TODO: Move to some constants
UMLS_CONFIG_PREFIX = 'umls.'

# The default number of rows to buffer before committing to the target database
COMMIT_EVERY = 100000

# These are tables that might not exist in the UMLS and we should not error
# out if they don't
OPTIONAL_TABLES = [
    'SRDEF',
    'SRFIL',
    'SRFLD',
    'SRSTR',
    'SRSTRE1',
    'SRSTRE2',
    'unique_umls_cui'
]

# These are tables that should exist in the UMLS and if they do not it is
# an error
EXPECTED_TABLES = [
    'MRCOLS',
    'MRCONSO',
    'MRCUI',
    'MRCXT',
    'MRDEF',
    'MRDOC',
    'MRFILES',
    'MRHIER',
    'MRHIST',
    'MRMAP',
    'MRRANK',
    'MRREL',
    'MRSAB',
    'MRSAT',
    'MRSMAP',
    'MRSTY',
    'MRXNS_ENG',
    'MRXNW_ENG',
    'MRAUI',
    'MRXW_BAQ',
    'MRXW_CHI',
    'MRXW_CZE',
    'MRXW_DAN',
    'MRXW_DUT',
    'MRXW_ENG',
    'MRXW_EST',
    'MRXW_FIN',
    'MRXW_FRE',
    'MRXW_GER',
    'MRXW_GRE',
    'MRXW_HEB',
    'MRXW_HUN',
    'MRXW_ITA',
    'MRXW_JPN',
    'MRXW_KOR',
    'MRXW_LAV',
    'MRXW_NOR',
    'MRXW_POL',
    'MRXW_POR',
    'MRXW_RUS',
    'MRXW_SCR',
    'MRXW_SPA',
    'MRXW_SWE',
    'MRXW_TUR',
    'AMBIGSUI',
    'AMBIGLUI',
    'DELETEDCUI',
    'DELETEDLUI',
    'DELETEDSUI',
    'MERGEDCUI',
    'MERGEDLUI'
]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UMLS copy ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # Set the copy operation going
    copy_db(
        args.source_db,
        args.target_db,
        verbose=args.verbose,
        commit_every=args.commit_every
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(
        description="Copy UMLS from one database to another"
    )

    # The wide format UKBB data fields file
    parser.add_argument(
        'source_db',
        type=str,
        help="This is either a path to a config file with a section heading"
        " in the config file detailing the connection options to the source"
        " database, delimited from the config file path with a | (pipe), or "
        " it is a path to an SQLite database file."
    )
    parser.add_argument(
        'target_db',
        type=str,
        help="This is either a path to a config file with a section heading"
        " in the config file detailing the connection options to the target"
        " database, delimited from the config file path with a | (pipe), or "
        " it is a path to an SQLite database file"
    )
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output and progress monitor")
    parser.add_argument(
        '-c', '--commit-every',  type=int, default=COMMIT_EVERY,
        help="When copying data commit to the target database --commit-every"
        " rows, default={0}".format(COMMIT_EVERY)
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Required files
    for db in ['source_db', 'target_db']:
        # Process the connection arguments into an SQLAlchemy sessionmaker
        sm = _get_db_connection(getattr(args, db))
        setattr(args, db, sm)

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_db_connection(db_arg):
    """
    Take a database connection argument and evaluate it to see if it is a file
    path to an SQLite database or a path to a connection file

    Parameters
    ----------
    db_arg : str
        If the db_arg is delimited by a | pipe then it is assumed to be a
        config file otherwise it is treated as an SQLite database. If it is
        not delimited by a | and the file ends in `ini`, `cnf`, `cfg`. Then
        this is also an error as it is likely that the user has forgotten the
        the delimited section header

    Returns
    -------
    sessionmaker : :obj:`sqlalchemy.orm.Sessionmaker`
        An `sqlalchemy.orm.Sessionmaker` object with which to create sessions.
        This is bound to an `sqlalchemy.engine.Engine` object for the database
        which can be retrieved with the `sessionmaker().get_bind()` method

    Raises
    ------
    ValueError
        If there is a single parameter and it looks like a config file. Or if
        there is a delimited parameter that does not look like a config file.
    """
    # Attempt to split on the delimiter
    params = re.split(r'\s*\|\s*', db_arg)

    # Make sure any relative paths are expanded to full paths
    params[0] = os.path.expanduser(params[0])

    # Make sure that is does not look like a config files
    looks_like_ini = _check_arg_extension(params[0])

    # Will hold a SQLAlchemy Sessionaker object
    sm = None

    # There are no delimited so treat as an SQLite database path
    if len(params) == 1 and looks_like_ini is True:
        # Could be an ini file
        raise ValueError(
            "you have a single parameter that could be an ini file, SQLite"
            " databases usually end with a .db extension"
        )
    elif len(params) == 1:
        # Treat as SQLite and setup the sessionmaker
        db_url = 'sqlite:///{0}'.format(params[0])
        engine = create_engine(db_url)
        sm = sessionmaker(bind=engine)
    elif len(params) == 2 and looks_like_ini is False:
        # Probably not an ini file
        raise ValueError(
            "your config file should end with one of the known ini file"
            " extensions: {0}".format(KNOWN_INI_EXT)
        )
    elif len(params) == 2:
        # Look up parameters in the config file
        conn_args = sqlalchemy_helper.get_sqlalchemy_connection_info(
            params[0], params[1], UMLS_CONFIG_PREFIX
        )

        # Generate a sessionmaker and assign to the arguments. The database
        # must exist if it is defined in a config file
        sm = sqlalchemy_helper.get_database_connection(
            conn_args, UMLS_CONFIG_PREFIX, must_exist=True
        )
    else:
        # No idea what is going on in this case!
        raise ValueError(
            "unexpected number of parameters: {0}:{1}".format(
                db, len(params)
            )
        )
    return sm


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _check_arg_extension(file_path):
    """
    Test to see if the argument has any of the known ini config file extensions
    this is used as a sanity check on the argument to determine if it is a
    config file for a database. The current extensions tested are `ini`, `cnf`
    and `cfg`.

    Parameters
    ----------
    file_path : str
        A file path to test the extension

    Returns
    -------
    looks_like_ini : bool
        `True` if the file_path looks has a known `ini` extension `False` if
         not
    """
    for ext in KNOWN_INI_EXT:
        if file_path.endswith(ext):
            return True
    return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def copy_db(source, target, verbose=False, commit_every=COMMIT_EVERY):
    """
    Copy the source database to the target database. This uses SQLAlchemy
    reflection and dynamic creation of ORM objects for each table to be
    created. A by product of this is that many tables in the UMLS do not have
    a primary key column. The SQLAlchemy ORM requires that one exists.
    Therefore, for those tables with no primary_key, one is added as the
    first column. The name for the primary key column is <TABLENAME>_ID.

    Parameters
    ----------
    source : :obj:`sqlalchemy.orm.Sessionmaker`
        An `sqlalchemy.orm.Sessionmaker` object with which to create sessions
        for the source database. This is bound to an `sqlalchemy.engine.Engine`
        object for the database which can be retrieved with the
        `sessionmaker().get_bind()` method.
    target : :obj:`sqlalchemy.orm.Sessionmaker`
        An `sqlalchemy.orm.Sessionmaker` object with which to create sessions
        for the target database. This is bound to an `sqlalchemy.engine.Engine`
        object for the database which can be retrieved with the
        `sessionmaker().get_bind()` method.
    verbose : bool, optional, default: False
        Output copying progress to STDERR
    commit_every : int, optional, default: 100000
        The number of rows to buffer before committing to the target database
    """
    # Get some sessions from the source and target databases
    source_session = source()
    target_session = target()
    source_dialect = source_session.bind.dialect.name
    target_dialect = target_session.bind.dialect.name
    # First get a list of all the table names from the source database so we
    # can determine that we have all the tables we require
    meta = MetaData()
    meta.reflect(bind=source_session.get_bind())
    table_names = list(meta.tables.keys())

    # We check all the tables first so we do not get any surprises mid copy
    required_tables = _check_tables(table_names)

    # TODO: Do two loops over the required tables, the first creating them
    #  the second filling them, this means that we see any dialect table
    #  creation issues before we start to load data
    for table in required_tables:
        sql_alchemy_table = meta.tables[table]

        orm_class = _get_orm_class(
            source_dialect,
            target_dialect,
            sql_alchemy_table
        )

        # Copy the table contents, note that I have set checkfirst=False
        # this will error out if the table already exists, I am not sure
        # what the error will be so I am just catching Exception and skipping
        # so we do not double write to a table.
        # TODO: Improve table creation
        try:
            sqlalchemy_helper.copy_table(
                source_session,
                sql_alchemy_table,
                target_session,
                to_table=orm_class,
                checkfirst=False,
                verbose=verbose,
                commit_every=commit_every
            )
        except Exception as e:
            warnings.warn(
                "could not copy table {1}: {0}".format(
                    e.args[0],
                    table
                )
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _check_tables(found_tables):
    """
    Check the found tables against what we expect and the optional tables. If
    any of the expected tables are missing then it is an error. If any of the
    optional tables are missing then we issue a warning. We also look for
    additional tables that are found but not in expected or optional tables,
    these might be schema changes and we will need to update our code.

    Parameters
    ----------
    found_tables : list of str
        A list of table names to check. These will have been reflected out
        from the source database

    Returns
    -------
    required_tables : list or str
        The tables that are scheduled to be copied from the source database to
        the target database, this includes both expected and optional tables
        and they are returned in the order that they are listed in expected
        and/or optional tables, as the order of copying may be important with
        respect to foreign key constraints. Expected tables are always listed
        before optional tables.

    Raises
    ------
    KeyError
        If one of the `expected_tables` is not in `found_tables`
    """
    required_tables = []
    for i in EXPECTED_TABLES:
        if i not in found_tables:
            raise KeyError("expected table not found: {0}".format(i))
        required_tables.append(i)

    for i in OPTIONAL_TABLES:
        if i not in found_tables:
            warnings.warn("optional table not found: {0}".format(i))
        required_tables.append(i)

    for i in found_tables:
        if i not in EXPECTED_TABLES and i not in OPTIONAL_TABLES:
            warnings.warn(
                "not in expected/optional tables, this could "
                "be a schema change: {0}".format(i)
            )
    return required_tables


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_orm_class(source_dialect, target_dialect, table):
    """
    Add SQLAlchemy column objects to the attributes that will be used to
    generate the SQLAlchemy ORM class. There is a complication with the UMLS in
    that not all the tables have a primary key. So this is checked and a
    primary key column is added to the table if not present already.

    Parameters
    ----------
    table : :obj:`sqlalchemy.Table`
        The table we want to grab the columns from and check for the presence
        of a primary key column

    Returns
    -------
    orm_class : obj:`sqlalchemy.declarative.Base`
        An ORM class that inherits from the SQLAlchemy declarative base, this
        can be used to create the table in the target and for bulk updates
    """
    # We want to dynamically build an ORM class using type(), so we
    # initialise the arguments with the table name
    attrs = {'__tablename__': table.name}

    # Then determine if the table already has a primary key column, if not
    # then we create one
    has_pk = has_primary_key(table)
    if has_pk is False:
        # if we create a primary key it will be called <TABLENAME>_ID
        # TODO: Should really check that a column with the primary key name
        #  does not already exist
        attrs['{0}_ID'.format(table.name)] = Column(
            Integer,
            nullable=False,
            autoincrement=True,
            primary_key=True
        )
    # pp.pprint(table)
    # print(has_pk)
    # Now all the other columns
    for col in table.columns:
        col.table = None

        # TODO: Sort out a COLLATION map between various databases
        # https://stackoverflow.com/questions/56972339
        try:
            # TODO: See if there is a COLLATION map anywhere
            if col.type.collation is not None:
                if col.type.collation.endswith('_bin') \
                   and target_dialect =='sqlite':
                    col.type.collation = 'NOCASE'
                else:
                    warnings.warn(
                        "collation removed for {0}.{1}: {2}".format(
                            table.name, col.name, col.type.collation
                        )
                    )
                    col.type.collation = None
            attrs[col.name] = col
        except AttributeError:
            # not all col.type have the collation attribute
            attrs[col.name] = col

        # MySQL doubles to floats, this is ok for the limited example
        # or the UMLS but need to generalise or find a proper SQLAlchemy
        # way of doing it
        # TODO: Look for generalisable dialect mapping
        if isinstance(col.type, DOUBLE):
            attrs[col.name] = Column(Float, nullable=col.nullable)

    # pp.pprint(attrs)
    return type(table.name, (Base,), attrs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def has_primary_key(table):
    """
    Parameters
    ----------
    table : :obj:`sqlalchemy.Table`
        The table we want to grab the columns from and check for the presence
        of a primary key column

    Returns
    -------
    has_primary_key : bool
        `True` if the table has a primary key `False` if not
    """
    primary_key = False
    for col in table.columns:
        primary_key |= col.primary_key

    return primary_key


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
