# Importing UMLS into MySQL
Some brief and incomplete notes. See `README.md` for full explaination: 

The file `create_umls_database.sh` takes the username, host, port and database name as arguments and executes the statement to create the database with unicode enabled (and for collation as well). It assumes that there is a password on the database and this will be asked on the command line. Usage:

    ./create_umls_database.sh root localhost 3306 umls_2016ab

For useful information on loading the Metathesaurus subset into MySQL, see [here](http://www.nlm.nih.gov/research/umls/implementation_resources/scripts/index.html). But essentially, copy the database load scripts and sql scripts into the directory with the load files in it. The "NET" ones are for the semantic network and the others for the Metathesaurus. Add the data base name, user, password and database path to the bash script. Make sure the bash script is executable and run it.

