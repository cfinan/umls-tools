Welcome to umls-tools
=====================

`umls-tools <https://gitlab.com/cfinan/umls-tools>`_ is a package containing various modules and scripts for interacting with the UMLS. This has been put together from various ad-hoc solutions developed over the years and I hope to gradually improve things as an when I have to return to them. When usable, I will add documentation to the API docs and any cmd-line utilities. Currently only the metamap sub-package could be considered properly off the shelf usable.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started
   installing_umls_db
   setup_config

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   schema
   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
