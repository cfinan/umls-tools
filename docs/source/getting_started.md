# umls-tools package #
__version__: `0.1.0a1`

## Overview
umls-tools is a package containing various modules and scripts for interacting with the UMLS. This has been put together from various ad-hoc solutions developed over the years and I hope to gradually improve things as an when I have to return to them. When usable, I will add documentation to the API docs and any cmd-line utilities. Currently only the metamap sub-package could be considered properly off the shelf usable.

## Installation
Currently there is no online documentation in of `umls_tools`. However, offline static webpages can be accessed at `./html/umls_tools/index.html`, where `.` is referenced from the root of the repository.

## Installation instructions
**check the requirments for my non-public repos**

At present, umls-tools is undergoing development and no packages exist yet on PyPi or in Conda. Therefore it is recommended that umls-tools is installed in either of the two ways listed below.

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install umls-tools as an editable install also via pip:

```
python -m pip install --upgrade -r requirements.txt
```

For an editable (developer) install run the command below from the root of the `umls_tools` repository:
```
python -m pip install -e .
```

This will also several console scripts into your path.

### Installation using conda dependencies
A conda environment is provided in a `yaml` file in the directory `./conda_env`. A new conda environment called `umls_tools` can be built using the command:

```
# From the root of the umls_tools repository
conda env create --file conda_env/conda_create.yml
```

To add to an existing environment use:
```
# From the root of the umls_tools repository
conda env update --file conda_env/conda_update.yml
```

For an editable (developer) install run the command below from the root of the `umls_tools` repository:
```
python -m pip install -e .
```

This will also several console scripts into your path
