===========
UMLS Schema
===========

The UMLS does not have a normalised schema, however there are relationships between the various tables. These are documented below. Also for more information see the UMLS documentation at the `NLM <https://www.ncbi.nlm.nih.gov/books/NBK9685/>`_

.. include:: ./data_dict/umls-tables.rst
