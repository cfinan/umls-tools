Setup sqlalchemy config file
============================

    Please note, so far the ``umls-build`` :ref:`script <umls_build>` has only been tested on SQLite and MariaDB, the ORM definitions might need to be adapted slightly for other database backends, please let me know if you have any issues

If you are not using a simple SQLite database file to store the UMLS, you will probably want to store your database connection parameters in a config file rather then passing them on the command line.

For this, a small package called `sqlalchemy-config <https://cfinan.gitlab.io/sqlalchemy-config/index.html>`_ is used. This allows the SQLAlchemy database connection parameters to exist in a configuration file (``.ini`` or ``.cnf`` file).

This illustrates some simple configuration for both MySQL and SQLite (yes you can put that in the config as well).

By default, the code in this module will look for a config file called ``.db.cnf`` situated in the root of your home directory. The location can be changed when using scripts/API calls.

It will also look for a default section name called ``umls`` (i.e. ``[umls]``), this can also be changed. For example, you might want to specify connection parameters for different UMLS versions.

The keys containing the connection parameters also should have the prefix ``umls.``. This is shown below.

Obviously, you should make sure that this file is only readable by you!

.. code-block:: console

   $ cat ~/.db.cnf
   [umls]
   # Latest, localhost
   umls.url = mysql+pymysql://user:password@127.0.0.1:3306/umls_2022ab?charset=utf8

   [umls_2020aa_sqlite]
   # Old SQLite version
   umls.url = sqlite:///path/to/umls_2020aa.db


