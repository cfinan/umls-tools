``umls_tools.neo4j`` sub-package
================================

This contains tools for extracting node and relationship data from the UMLS relational data base for import into Neo4j (see :ref:`bash scripts <bash-scripts>`).

``umls_tools.neo4j.extract`` module
-----------------------------------

.. autofunction:: umls_tools.neo4j.extract::build_files
