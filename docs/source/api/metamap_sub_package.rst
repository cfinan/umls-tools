``umls_tools.metamap`` sub-package
==================================

``umls_tools.metamap.metamap`` module
-------------------------------------

.. automodule:: umls_tools.metamap.metamap
   :members:
   :undoc-members:
   :show-inheritance:

``umls_tools.metamap.objects`` module
-------------------------------------

.. automodule:: umls_tools.metamap.objects
   :members:
   :undoc-members:
   :show-inheritance:

``umls_tools.metamap.processes`` module
---------------------------------------

.. automodule:: umls_tools.metamap.processes
   :members:
   :undoc-members:
   :show-inheritance:
