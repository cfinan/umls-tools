``umls_tools`` package
======================

``umls_tools.mapping`` module
-----------------------------

.. autofunction:: umls_tools.mapping::map_file


``umls_tools.query`` module
---------------------------

.. autoclass:: umls_tools.query.CuiMapper
.. autofunction:: umls_tools.query::get_cui
.. autofunction:: umls_tools.query::get_unique_cui
.. autofunction:: umls_tools.query::get_ranks

.. include:: ../data_dict/umls-orm.rst

``umls_tools.orm_mixin`` module
-------------------------------

.. autoclass:: umls_tools.orm_mixin.TermIndexLookupMixin
.. autoclass:: umls_tools.orm_mixin.TermIndexMapMixinInt
.. autoclass:: umls_tools.orm_mixin.TermIndexMapMixinStr
