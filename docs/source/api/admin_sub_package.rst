``umls_tools.admin`` sub-package
================================

``umls_tools.admin.build`` module
---------------------------------

.. autofunction:: umls_tools.admin.build::build_umls

``umls_tools.admin.index`` module
---------------------------------

.. automodule:: umls_tools.admin.index
   :members:
   :undoc-members:
   :show-inheritance:


``umls_tools.admin.unique_cui`` module
--------------------------------------

.. autofunction:: umls_tools.admin.unique_cui::create_unique_cui

``umls_tools.admin.output_sab`` module
--------------------------------------

.. autofunction:: umls_tools.admin.output_sab::output_sab
