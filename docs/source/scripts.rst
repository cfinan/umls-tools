====================
Command line scripts
====================

There are several command line scripts that are installed with the package. These are detailed below.

Building the UMLS database
==========================

.. _umls_build:

``umls-build``
--------------

    Please note, so far the ``umls-build`` has only been tested on SQLite and MariaDB, the ORM definitions might need to be adapted slightly for other database backends, please let me know if you have any issues.

.. argparse::
   :module: umls_tools.admin.build
   :func: _init_cmd_args
   :prog: umls-build

.. _umls_unique:

``umls-unique-cui``
-------------------

    Please note, so far the ``umls-unique-cui`` has only been tested on SQLite and MariaDB, the ORM definitions might need to be adapted slightly for other database backends, please let me know if you have any issues.

.. argparse::
   :module: umls_tools.admin.unique_cui
   :func: _init_cmd_args
   :prog: umls-unique-cui

.. _umls_index:

``umls-index``
--------------

    Please note, so far the ``umls-index`` has only been tested on SQLite and MariaDB, the ORM definitions might need to be adapted slightly for other database backends, please let me know if you have any issues.

.. argparse::
   :module: umls_tools.admin.index
   :func: _init_cmd_args
   :prog: umls-index

.. _umls_extract:

``umls-neo4j-extract``
----------------------

.. argparse::
   :module: umls_tools.neo4j.extract
   :func: _init_cmd_args
   :prog: umls-neo4j-extract

Extracts from the UMLS
======================

.. _umls_sab_output:

``umls-output-sab``
-------------------

.. argparse::
   :module: umls_tools.admin.output_sab
   :func: _init_cmd_args
   :prog: umls-output-sab

Mapping against the UMLS
========================

.. _umls_mapper:

``umls-mapper``
---------------

.. argparse::
   :module: umls_tools.mapping
   :func: _init_cmd_args
   :prog: umls-mapper

Output columns
~~~~~~~~~~~~~~

This script is designed to annotate an existing data set. So all the columns below will appear after the original data columns. Note that this will cause the original data columns to be replicated as a single input term may map to many UMLS terms, unless the ``--top-rank`` option is used.

.. include:: ./data_dict/umls-mapper.rst

.. _umls_code_lookup:

``umls-code-lookup``
--------------------

.. argparse::
   :module: umls_tools.codes
   :func: _init_cmd_args
   :prog: umls-code-lookup

Output columns
~~~~~~~~~~~~~~

This script is designed to annotate an existing data set. So all the columns below will appear after the original data columns. Note that this will cause the original data columns to be replicated as a single input term may map to many UMLS terms, unless the ``--top-rank`` option is used.

.. include:: ./data_dict/umls-code-lookup.rst


.. _bash-scripts:

BASH scripts
============

In addition to the Python command-line scripts that are installed when the package is installed. There are also some bash administrative scripts located in ``./resources/bin``. Please note these will not be installed when you install via clone & pip or a conda install. If using conda you will have to clone the repo and add the ``./resources/bin`` directory to your PATH.

These scripts will require two bash libraries to be in your PATH.

1. ``shflags`` - `Used <https://github.com/kward/shflags>`_ to manage bash command line arguments.
2. ``bash-helpers`` - `This <https://gitlab.com/cfinan/bash-helpers>`_ wraps some handle bash functions.

.. _full-build:

``build-umls-neo4j.sh``
-----------------------

Run the entire UMLS Neo4j build process. This will generate the container and extract the data from the UMLS and load into a Neo4j database running inside the container. The container must not already exist. The data is extracted into a data sub-directory within ``<neo4j_data_dir>``. If ``<neo4j_data_dir>`` does not exist then it is created. The Neo4j container is created with user permissions and no authentication.

The ``<db_connection>`` parameter can specify a file (i.e. an SQLite database), or an SQLAlchemy connection URL. If You do not want to place connection strings on the command line (for security reasons), you can specify a section name (with the ``--config-section`` argument) in a connection file (``.ini``) that points to a section with the connection URL. The URL should be given with the key 'umls.url' i.e.

.. code-block::

   [umls_2022aa_mysql]
   umls.url = mysql+pymysql://user:password@hostname:port/umls_2022aa?charset=utf8

The location of the config file can be given with the ``--config`` parameter. Please note, depending on the number of SABs being extracted, this could take a while to run.

Also, after running the build process you will want to create some node indexes, for check see :ref:`create Neo4j indexes <neo4j-idx>`.

Example command
~~~~~~~~~~~~~~~

.. code-block::

   build-umls-neo4j.sh --neo4j-db-name "umls-2022aa" \
                       --config-section umls_2022aa_mysql \
                       --config ~/.db.cnf \
                       --sabs "RCD,MSH,SNOMEDCT_US,ICD10,ICD10CM,ICD10PCS,MDR,SNM2,SNMI" \
                       -v \
                       --container "umls_2022aa" \
                       /data/umls/neo4j/umls_2022aa

Usage
~~~~~

.. code-block::

   USAGE: build-umls-neo4j.sh [flags] <neo4j_data_dir> <db_connection>

   flags:
     -c,--container:  The name of the docker container to use, must not exist already (default: 'umls_neo4j')
     -i,--image:  The Neo4j docker image to use (default: 'neo4j:latest')
     -b,--bolt-port:  The Neo4j bolt authentication port to use (default: 7687)
     -w,--web-port:  The Neo4j web-interface port to use (default: 7474)
     -s,--sabs:  The source vocabularies to extract for. This should be a comma separated list with no spaces, if not defined the this defaults to all available SABs (default: '')
     -T,--tmp:  The tmp directory location to use, if not defined this will use the system temp location (default: '')
     -v,--[no]verbose:  Output progress (default: false)
     -f,--config:  The location of the config file with the UMLS relational database parameters (default: '')
     -e,--config-section:  The section name in the config file where the UMLS connection details are stored (default: '')
     -n,--neo4j-db-name:  The name of the Neo4j graph DB, this should use dashes not underscored (default: 'umls')
     -h,--help:  show this help (default: false)


``create-neo4j-container.sh``
-----------------------------

Download the docker image and create a new container from it. This assumes that a container of that name does not already exist. This will also setup mount points for the neo4j database, so requires the user to give a directory name in which to place the data (must exist). The container is created with user permissions and no authentication.

Please note, if you are using ``build-umls-neo4j.sh`` then you do not need to run this.

Usage
~~~~~

.. code-block::

   USAGE: create-neo4j-container.sh [flags] <neo4j_data_dir>
   flags:
     -c,--container:  The name of the docker container to use, must not exist already (default: 'umls_neo4j')
     -i,--image:  The Neo4j docker image to use (default: 'neo4j:latest')
     -b,--bolt-port:  The Neo4j bolt authentication port to use (default: 7687)
     -w,--web-port:  The Neo4j web-interface port to use (default: 7474)
     -h,--help:  show this help (default: false)


``connect-neo4j-container.sh``
------------------------------

Connect to an interactive terminal of the Neo4j docker container.

Usage
~~~~~

.. code-block::

   USAGE: connect-neo4j-container.sh [flags]
   flags:
     -c,--container:  The name of the docker container to use, will be started if not already (default: 'umls_neo4j')
     -h,--help:  show this help (default: false)


.. _neo4j-idx:

``create-neo4j-indexes.sh``
---------------------------

A wrapper script that is designed to create indexes on nodes for the UMLS graph database. This is copied to the plugins directory of ``<neo4j_data_dir>`` when ``build-umls-neo4j.sh`` is run.

After the build is completed, you should start the docker container and run this script in either of the ways below:

From within an interactive session on the docker container.

.. code-block::

   /plugins/create-neo4j-indexes.sh

From the host system outside of the docker container.

.. code-block::

   docker exec -it <container_name> /plugins/create-neo4j-indexes.sh


``load-umls-neo4j.sh``
----------------------

Load extracted UMLS data into a non-existent Neo4j database. This is designed to be run within the docker container. It has two assumptions:

1. You have the files you want to load mounted within an import directory at ``/var/lib/neo4j/import``.
2. The node files have the suffix ``*_nodes.txt`` (uncompressed) and the relationships have the suffix ``*_rel.txt`` (uncompressed).

This script in either of the ways below. These assume you have copied the script to the container ``/plugins`` directory. However, if you are running the :ref:`full build <full-build>` process then you do not need to run this as it is run as part of that process.

From within an interactive session on the docker container.

.. code-block::

   /plugins/load-umls-neo4j.sh

From the host system outside of the docker container.

.. code-block::

   docker exec -it <container_name> /plugins/load-umls-neo4j.sh

.. _neo4j-stop:

``stop-neo4j-container.sh``
---------------------------

Stop the Neo4j docker container. This does not use ``docker stop`` as it causes the container to exit with a code 137 and then it can never be restarted again without crashing. Instead, this docker executes the neo4j stop command that stops the container cleanly. This seems to be a bug in the Neo4j docker image v5.4.0 and may not effect all the images. If you do not have any of these issues then please just use ``docker stop <container_name>``.

Usage
~~~~~

.. code-block::

      USAGE: stop-neo4j-container.sh [flags]
      flags:
      -c,--container:  The name of the docker container to use, must be running already (default: 'umls_neo4j')
      -h,--help:  show this help (default: false)

``switch-neo4j-database.sh``
----------------------------

Switch the Neo4j database used by the docker container. This should be run on the host system. This adjusts the default database in the config file (``/var/lib/neo4j/conf/neo4j.conf`` mounted into ``<neo4j_data_dir>/conf`` on the host). It will first stop the docker container (if needed), adjust the config and then restart again (if needed).

This is used to get around the issues with the community edition of the Neo4j docker where you build the database with the import tool and can't use ``CREATE DATABASE`` to let Neo4j know about it. You also can't adjust the neo4j.conf file from within the docker as it resets unless over mounted. Ideally you could accomplish this with the ``--env NEO4j_initial_dbms_default__database=umls`` when creating the container but it does not seem to work.

If you are running the :ref:`full build <full-build>` process then you do not need to run this as it is run as part of that process.

Usage
~~~~~

.. code-block::

   USAGE: switch-neo4j-database.sh [flags] <new_db_name> <conf_dir>
   flags:
     -c,--container:  The name of the docker container to use, must not exist already (default: 'umls_neo4j')
     -h,--help:  show this help (default: false)


.. _create-mysql:

``init-empty-umls-mysql.sh``
----------------------------

Create an empty MySQL/MariaDB database with the correct character set and collation for the UMLS. Note that this assumes a password is set and it will be asked on the command line.

Usage
~~~~~

.. code-block::

   USAGE: init-empty-umls-mysql.sh [flags]
   flags:
     -u,--user:  The MySQL/MariaDB username (default: 'umls_user')
     -H,--host:  The MySQL/MariaDB hostname (default: '127.0.0.1')
     -P,--port:  The MySQL/MariaDB port (default: '3306')
     -D,--database:  The MySQL/MariaDB UMLS database name (default: 'umls')
     -h,--help:  show this help (default: false)
