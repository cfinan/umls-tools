# Local installation of the UMLS as a database
This requires an account and the data can be downloaded [here](https://www.nlm.nih.gov/research/umls/licensedcontent/umlsknowledgesources.html). There are two sets of instructions below. The first is the latest to build the whole of the UMLS from downloadable flat files and a database build script in this repository. The second is for the `Full Release` version that has to be subset with MetamorphoSys.

It is recommended that you build from the flat files as this will allow greater flexibility of the database back end.

If you are using a MySQL/MariaDB back end, then there is a small [bash script](https://cfinan.gitlab.io/umls-tools/scripts.html#create-mysql) to create an empty database with the correct character set and collation for the UMLS. Also please see the general [bash scripts](https://cfinan.gitlab.io/umls-tools/scripts.html#bash-scripts) documentation.

## Building the database via the flat files
This is slightly easier than using MetamorphSys but is not so flexible. First download the UMLS Metathesaurus Files from [here](https://www.nlm.nih.gov/research/umls/licensedcontent/umlsknowledgesources.html). The download link should be in a table with UMLS Metathesaurus Files in it. The downloaded file will be a zip archive having the structure `umls-<YYYY><RELEASE>-metathesaurus.zip`. You do not need to unzip this and you should keep the file name the same.

In future (but not currently implemented) will also need to download the semantic network files as these are not included directly with the UMLS files. These can be found [here](https://lhncbc.nlm.nih.gov/semanticnetwork/download.html). There is a tab separated file and the pipe separated file. Download these and please do not change their delimiters.

For details please see the [umls-build](https://cfinan.gitlab.io/umls-tools/scripts.html#umls-build) script.

## Building the database via MetamorphSys
These instructions are valid for the `UMLS2020AA` release being installed on a GNU Linux system. UMLS downloads have the structure `umls-<YYYY><RELEASE>-full.zip`, where the first half of the year releases are `AA` and the second half of the year releases are `AB`. As the download is a zip file i.e. `umls-2020AA-full.zip`. This needs to be decompressed for example with the command `unzip  umls-2020AA-full.zip` and doing so will create a directory called `2020AA-full`.

### Loading the data into MySQL
#### A note on MariaDB on Linux
If you are installing onto a MariaDB server in a UNIX environment then it will use UNIX sockets as the default authentication. This will mean that many of the scripts to load into MySQL will not work properly. In order to work around this. If you have access to the root account, it is recommended that you create a user with access via TCP and not unix sockets. The command below will create a user with root like privileges that can access the locally installed database only:

```
$ sudo su
[sudo] password for user12345:
root@lardarse:/home/user12345/code# mysql
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 36
Server version: 10.3.22-MariaDB-1ubuntu1 Ubuntu 20.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> GRANT ALL PRIVILEGES ON *.* TO 'admin'@'127.0.0.1' IDENTIFIED BY 'my_password';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> exit
Bye
```

If this technique is used then the host name will need to be added to the `populate_mysql_db.sh` and `populate_net_mysql_db.sh` scripts. For example, add `host="127.0.0.1"` to the variables at the top of the file and `-h "$host"` to the individual MySQL commands. See below for more details.

#### Loading
The subsetting of the data wrote some database load script and SQL fro MySQL. These require a unicode (UTF8) encoded MySQL database to be created. Assuming that you have database creation privileges, this can be created with a SQL command below:

```
CREATE DATABASE IF NOT EXISTS umls_2020aa CHARACTER SET utf8 COLLATE utf8_unicode_ci;
```

There is also a small bash script to create the database that can be run instead, an example is shown below for a locally installed MySQL instance.

```sh
./resources/bin/create_umls_database.sh admin localhost 3306 umls_2020aa
```

The UMLS database load scripts then need to be modified slightly to take your database parameters. Identify the following lines in the script `UMLS2020-AA/2020AA/META/populate_mysql_db.sh` and edit them for your parameters:

```sh
MYSQL_HOME=<path to MYSQL_HOME>
user=<username>
password=<password>
db_name=<db_name>
```

Here is an example config, this assumes that the MySQL binary is located at `/usr/bin/mysql` and the user has no password. If your database is setup to authenticate via the UNIX socket then you will also need to remove the `-u` and `-p` flags (and arguments) in the script (there should be 3 MySQL commands in there). Note that this will be saving your password un-encrypted so do not commit to git and make sure it is only readable by you:

```sh
MYSQL_HOME=/usr
user=admin
password=my_password
db_name=umls_2020aa
```

Finally make the script executable `chmod +x populate_mysql_db.sh` and run it:

```sh
./populate_mysql_db.sh
```

After this has loaded, repeat the process with `UMLS2020-AA/2020AA/NET/populate_net_mysql_db.sh` to load the semantic network tables.

### Subsetting the data with MetamorphoSys
Within the directory, there is a further zip file called `mmsys.zip`. This contains the MetamorphoSys binary. MetamorphoSys is used to browse the UMLS flat files and create subsets of the flat files for resources that the user has permissions for. Unzip `mmsys.zip` with `unzip mmsys.zip`.

There are now several more files including some installers:

* `run_linux.sh`
* `run_mac.sh`
* `run64.bat`
* `run.bat`

These represent startup scripts for several for Linux, Mac and Windows operating systems. As we are using Linux, the MetamorphoSys application was launched with (issued in the `2020AA-full` directory):

```
./run_linux.sh
```

This will run a java application called MetamorphoSys and screen shot below should be displayed. Select `Install UMLS`:
<div>
<img width="50%" height="50%" src="./_static/images/intro_screen.png">
</div>

You will then be prompted for the source of your UMLS files. This will be the `2020AA-full` directory in this case and the destination of the subset files:
<div>
<img width="60%" height="60%" src="./_static/images/install_umls.png">
</div>


A dialog is then displayed asking you if you want to use an existing configuration or generate a new configuration. Select `New Configuration`:
<div>
<img width="70%" height="70%" src="./_static/images/config1.png">
</div>


A further dialog is then disaplyed asking what to use as the default selections for the subset. So, what ontologies/resources do you want your subset of the UMLS to contain. Many of the resources in the UMLS are covered by additional licences, however, `Level 0` resources are not and for users in a selected group of countries `SnomedCT (US)` is not either. If in doubt select the `Level 0` subset.
<div>
<img  width="70%" height="70%" src="./_static/images/config2.png">
</div>



Now various tabs of options should be displayed. The options relevant to loading into a MySQL database are under `Output Options` -> `Write Database Load Scripts`, make sure `MySQL 5.6` is set in the dropdown box. Additionally, under `Build browser index files` uncheck the `Build and sort browser index files (/indexes).` option as these are not needed for building the database.
<div>
<img width="70%" height="70%" src="./_static/images/config3.png">
</div>


Next you can review the `Source list` and select any other sources that you have licences for. The selection of resources on this tab can be quite confusing. As it is shown below, the highlighted (greyed out) resources are not included and the ones in white are. As `Select sources to EXCLUDE from subset` is checked we can click on the highlighted (excluded) resources to make them white and include them. Be sure to have `<CTRL>` pressed to maintain the current (de)selections. If you make a mistake you can reset with the `Reset` menu at the top of the window. Please only select resources you have licences for or those that do not require a licence.
<div>
<img  width="70%" height="70%" src="./_static/images/config4.png">
</div>


When you have finished, use the `Done` menu and this will bring up a progress box and start the subsets. When finished you should see a report like the one below:
<div>
<img  width="70%" height="70%" src="./_static/images/finish.png">
</div>


The subset files should be located in `UMLS2020-AA/2020AA/META`, they are all in rich release format `.RRF`. There are also a set of `.sql` scripts and a `MySQL` load script `populate_mysql_db.sh` in there. Additionally, `UMLS2020-AA/2020AA/NET` contains data files (with no extension) and database load scripts `populate_net_mysql_db.sh`.

## Building the ``UNIQUE_CUI`` table.
Some of the mapping functions in this package require a ``UNIQUE_CUI`` table to be built. This contains the top ranked AUIs for each CUI along with the occurrence and frequency of each CUI. Please see the [umls-unique-cui](https://cfinan.gitlab.io/umls-tools/scripts.html#umls-unique-cui) script for details.

## Building a Neo4j version of the UMLS
Traversing relationships is a natural process for a graph database such as Neo4j. Therefore, a set of scripts are available to extract the data from a UMLS relational database and build them into a Neo4j graph database located inside a docker container.

The Neo4j docker container seems a but temperamental and the community edition has some limitations. However, there is a full build [`bash script`](https://cfinan.gitlab.io/umls-tools/scripts.html#full-build) (and some other helper scripts) designed to navigate around some of the issues.

Currently this should be considered experimental and took a bit of time to get (almost) correct. However, Neo4j does perform very well for relationship queries. For full details of the issues and solutions see the [bash script](https://cfinan.gitlab.io/umls-tools/scripts.html#bash-scripts) documentation.
