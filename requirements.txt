beautifulsoup4
biopython
lxml
nltk
numpy>=1.18, <1.24
pandas
py2neo
pymysql
pytest>=5
requests
sqlalchemy>=2
sqlalchemy-utils
texttable
tqdm
git+https://gitlab.com/cfinan/sqlalchemy-config.git@master#egg=sqlalchemy_config
git+https://gitlab.com/cfinan/stdopen.git@master#egg=stdopen
git+https://gitlab.com/cfinan/pyaddons.git@master#egg=pyaddons
git+https://gitlab.com/cfinan/merge-sort.git@master#egg=merge_sort
